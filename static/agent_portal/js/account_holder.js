$(function(){
        $('#confirm_button').on('click', function(event){
            event.preventDefault();

           // alert('clicked');
           //switch to loading
           var base_path = '/rsp/api/holders/name/';
            var url = '';

            var bank_code_element = $('#bankcode :selected');
            var account_number_element = $('#accountnumber');



            if (!bank_code_element.val()){
                return alert('Please select a bank');
            }

            if (!account_number_element.val()){
                return alert('Please add beneficiary account number');
            }

            $('body').mask('Loading...');

            var bank_code = bank_code_element.val();
            var account_number = account_number_element.val();


            url = base_path + bank_code + '/' + account_number;



            $.get(url, function(data){

                var result = data.split("|")

                var name_holder = $('#nameholder');

               if($.trim(result[0]) == '0'){
                    //display error
                    $('body').unmask('Loading...');
                    alert('Incorrect account number or selected bank. Please check details and try again');

                }else{
                    $('body').unmask('Loading...');
                    $('#ben_bank_name').val(result[1]);
                    $('#bank_full_name').val(result[1]);
                    $('#confirm_button').replaceWith(result[1]);
                    $("#transfer_submit").removeAttr("disabled");
                   //display name and add name to the hiden input account_holders_name
                    //enable transfer money button
              }
            });

           //remove loading icon
           return false;
        });





    })