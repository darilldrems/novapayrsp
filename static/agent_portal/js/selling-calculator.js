 $(function(){



    var exchange_rates = {


    {{ agent.country.currency.name }}: {{ agent.country.currency.get_selling_price_exchange_rate }},

    };

    var currency_symbols = {

    {{ agent.country.currency.name }}: '{{ agent.country.currency.symbol }}',


    };

    var currency_abr = {

    {{ agent.country.currency.name }}: '{{ agent.country.currency.abbreviation }}',


    };


var currency_selected = document.querySelector('#currency');
var transfer = document.querySelector('[name="transfer_amount"]');
var receiver = document.querySelector('#receiver_amount');
var fee_and_rate = document.querySelector('#fee_result');
var exchange_rate = document.querySelector('#exchange_rate');
var savings = document.querySelector('#savings');

//alert(transfer.value);

currency.onchange = function(){
   calculate();
}

$('input[name="transfer_amount"]').on("input", null, null, calculate);


function calculate(){
    var amount = parseFloat(transfer.value);
    var fee = 0;
    //if( amount >= 100 && amount <= 400){
    //    var fee = amount * 3/100;
    //}
    //else if(amount >400 && amount <= 900){
    //    var fee = amount * 2/100;
    //}
    //else if(amount > 900 & amount <= 2000){
    //    var fee = 25;
    //}

    var amount_after_fee = amount;

    var amount_in_naira = amount_after_fee * parseInt(exchange_rates[currency_selected.value]);

    receiver.value = (parseFloat(amount_in_naira)).formatMoney(2, '.', ',');
    fee_and_rate.innerHTML =  currency_symbols[currency_selected.value] + fee;
    exchange_rate.innerHTML = "1 " + currency_abr[currency_selected.value] + " = " + exchange_rates[currency_selected.value] + "NGN";
    //savings.textContent = currency_symbols[currency_selected.value] + fee + " + hidden fees";
}


Number.prototype.formatMoney = function(c, d, t){
var n = this,
    c = isNaN(c = Math.abs(c)) ? 2 : c,
    d = d == undefined ? "." : d,
    t = t == undefined ? "," : t,
    s = n < 0 ? "-" : "",
    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
    j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };


    });