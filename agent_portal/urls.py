from django.views.generic import TemplateView, RedirectView
from django.conf.urls import patterns, url
from django.conf import settings

urlpatterns = patterns('agent_portal.views',
    url(r'^$', TemplateView.as_view(template_name='agent_portal/home_v2.html'), name="home"),
    url(r'^register/$', 'register_view', name="agent_register"),
    url(r'^reset/password/', 'reset_password_view', name='reset_password'),
    url(r'^login/$', 'login_step_one_view', name="agent_login"),
    url(r'^login_confirmation/$', 'login_step_two_view', name="login_step_two"),
    url(r'^dashboard/$', 'dashboard_view', name="dashboard"),
    url(r'^request_fund/$', 'request_fund_view', name="request_fund"),
    url(r'^transactions/(?P<type_>\w+)/$', 'transaction_view', name='transactions'),
    url(r'^transfer/$', 'transfer_view', name='transfer'),
    url(r'^otp/$', 'otp_view', name='otp_view'),
    url(r'^transfer/(?P<iden>\w+)/$', 'transfer_invoice_view', name='transfer_invoice_view'),
    url(r'^logout/$', 'logout_view', name='logout'),
    url(r'^rates/$', 'rate_view', name='rate'),
    url(r'^find-agents/$', 'find_agents_view', name='agents_finder'),
    url(r'^favicon\.ico$', RedirectView.as_view(url=settings.STATIC_URL + 'agent_portal/img/favicon.ico')),

)
