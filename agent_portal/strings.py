flash_prefix = 'flash_'

feedback_messages = {
    'successful_fund_request': '',
    'incorrect_otp': 'Incorrect OTP / or OTP expired. If it is already more than 3 minutes. Please start a new transfer',
    'failed_transfer': 'Sorry the transfer failed. Please try again later.',
    'transfer_not_found': 'Sorry transfer not found.',
    'amount_is_higher_than_maximum_allowed': 'Sorry. Transfer amount is too high. You are only allowed to send ',
    'account_balance_too_low': 'Sorry your account balance is too low',
    'something_went_wrong': 'Something went wrong. We are working hard to fix it',
    'success_registration_message': 'You registration is now successful. We will review and get back to you.',
    'inactive_account': 'Your account is not active yet. Please contact your MTO/Aggregator',
    'email_or_password_incorrect': 'username or password incorrect',

}