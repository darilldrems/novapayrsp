from django.shortcuts import render
from django.db import transaction
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import user_passes_test
from django.views.decorators.http import require_GET
from django.core.cache import cache
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.core.paginator import PageNotAnInteger, Paginator, EmptyPage

from .forms import UserForm, AgentForm, SecretForm, LoginForm, SecretLoginForm, FundForm, TransferForm, OtpForm, ResetPasswordForm
from .models import Secret, secret_questions, Fund, Transfer, ErrorLog, Country, Agent, Currency, Schedule
from .strings import feedback_messages
from .helpers import set_flash, permitted_agent, get_flash, send_sms, wrap_try_block_decorator


from rsp.bankcontroller import BankController
from sanctions_list.sanctionsreader import SanctionsReader

import pyotp
from decimal import Decimal



@wrap_try_block_decorator
@transaction.atomic
def register_view(request, template_name='agent_portal/register.html'):
    if request.method == 'GET':
        user_form = UserForm()
        agent_form = AgentForm()
        secret_form = SecretForm()

        return render(request, template_name,
                      {
                        'user_form': user_form,
                        'agent_form': agent_form,
                        'secret_form': secret_form,
                      })

    else:

        user_form = UserForm(request.POST)
        agent_form = AgentForm(request.POST)
        secret_form = SecretForm(request.POST)

        if user_form.is_valid() and agent_form.is_valid() and secret_form.is_valid():

            user = user_form.save(commit=False)
            user.is_active = False
            user.save()
            agent = agent_form.save(commit=False)
            agent.user = user
            agent.save()

            for i in range(4):
                q_i = 'question_' + str(i + 1)
                Secret.objects.create(agent=agent, question=secret_questions[i], answer=secret_form.cleaned_data.get(q_i))

            return render(request, template_name, {'message': feedback_messages.get('success_registration_message')})
        else:
            secret_form = SecretForm()
            return render(request, template_name,
                          {
                              'user_form': user_form,
                              'agent_form': agent_form,
                              'secret_form': secret_form,
                          })


@wrap_try_block_decorator
def login_step_one_view(request, template_name='agent_portal/login_step_one.html'):

    if request.method == 'GET':
        login_form = LoginForm()
        return render(request, template_name, {'form': login_form})
        pass
    else:
        login_form = LoginForm(request.POST)

        if login_form.is_valid():

            username = login_form.cleaned_data.get('username')
            password = login_form.cleaned_data.get('password')


            user = authenticate(username=username, password=password)

            if user:
                if user.is_active:
                    try:
                        login(request, user)
                        return HttpResponseRedirect(reverse('dashboard'))
                        # agent = user.agent
                        # request.session['step_two_agent'] = user.username
                        # cache.set(user.username, agent, 90)
                        # return HttpResponse('found the user')
                        return HttpResponseRedirect(reverse('login_step_two'))
                    except ObjectDoesNotExist:
                        return HttpResponseRedirect(reverse('home'))
                else:

                    return render(request, template_name, {
                        'error': feedback_messages.get('inactive_account'),
                        'form': login_form,
                    })
                    pass
            else:

                return render(request, template_name, {
                    'error': feedback_messages.get('email_or_password_incorrect'),
                    'form': login_form,
                })



                pass
        else:
            return render(request, template_name, {'form': login_form})


@wrap_try_block_decorator
def login_step_two_view(request, template_name='agent_portal/login_step_two.html'):
    if request.method == 'GET':
        q_form = SecretLoginForm()
        return render(request, template_name, {'form': q_form})
    else:
        q_form = SecretLoginForm(request.POST)

        if q_form.is_valid():
            username = request.session.get('step_two_agent', None)

            if username:
                agent = cache.get(username)
                if agent:
                    answer_is_correct = agent.is_secret_answer_correct(
                        q_form.fields.get('question').label,
                        q_form.cleaned_data.get('question'))

                    # return HttpResponse(agent.user.username+':'+q_form.fields.get('question').label+':'+q_form.cleaned_data.get('question')+':'+str(answer_is_correct))

                    # return HttpResponse(answer_is_correct)
                    if answer_is_correct:
                        login(request, agent.user)
                        return HttpResponseRedirect(reverse('dashboard'))
                    else:
                        #incorrect answer
                        #count the number of failed attempts and block ip after the tenth time
                        return HttpResponseRedirect(reverse('login_step_two'))
                else:
                    #Auth expires after 90 secs. Please try to login again
                    # return HttpResponse("here")
                    return HttpResponseRedirect(reverse('agent_login'))
            else:
                #please login first
                return HttpResponseRedirect(reverse('agent_login'))

            pass
        else:
            #invalid form data
            return render(request, template_name, {'form': q_form})
        pass


@wrap_try_block_decorator
@user_passes_test(permitted_agent, login_url='/login')
@require_GET
def dashboard_view(request, template_name='agent_portal/dashboard.html', page_name='dashboard'):

    agent = request.user.agent
    recent_transfers = agent.latest_transfers()
    recent_funds = agent.latest_funds()
    schedules = Schedule.objects.filter(mto=request.user.agent.mto)

    return render(request, template_name, {
        'page_name': page_name,
        'agent': agent,
        'transfers': recent_transfers,
        'funds': recent_funds,
        'error': get_flash(request, 'error'),
        'message': get_flash(request, 'message'),
        'schedules': schedules,
    })


@wrap_try_block_decorator
@user_passes_test(permitted_agent, login_url='/login')
def transfer_view(request, template_name='agent_portal/transfer.html', page_name='transfer'):
    if request.method == 'GET':
        transfer_form = TransferForm()
        schedules = Schedule.objects.filter(mto=request.user.agent.mto)
        context_data = {
            'page_name': page_name,
            'agent': request.user.agent,
            'form': transfer_form,
            'schedules': schedules,
        }
        return render(request, template_name, context_data)

    else:
        if request.user.agent.account_balance < request.user.agent.country.currency.minimum_per_transfer:
            #set flash account balance too low for transfer
            set_flash(request, 'error', feedback_messages.get('account_balance_too_low'))
            return HttpResponseRedirect(reverse('dashboard'))

        transfer_form = TransferForm(request.POST)

        if transfer_form.is_valid():
            amount = Decimal(transfer_form.cleaned_data.get('amount_in_currency'))

            if request.user.agent.account_balance < amount:
                set_flash(request, 'error', feedback_messages.get('account_balance_too_low'))
                return HttpResponseRedirect(reverse('dashboard'))

            if amount > request.user.agent.country.currency.maximum_per_transfer:
                set_flash(request, 'error', feedback_messages.get('amount_is_higher_than_maximum_allowed') +
                                            str(request.user.agent.country.currency.maximum_per_transfer) +
                                            request.user.agent.country.currency.symbol)
                return HttpResponseRedirect(reverse('dashboard'))

            if amount < request.user.agent.country.currency.minimum_per_transfer:
                set_flash(request, 'error', feedback_messages.get('amount_is_lower_than_minimum_allowed') +
                                            str(request.user.agent.country.currency.minimum_per_transfer) +
                                            request.user.agent.country.currency.symbol)
                return HttpResponseRedirect(reverse('dashboard'))

            transfer = transfer_form.save(commit=False)
            transfer.prepare_transfer(request.user.agent)
            # transfer.currency = request.user.agent.country.currency.name
            # transfer.agent = request.user.agent
            # fx_rate = request.user.agent.country.currency.exchange_rate
            # transfer.exchange_rate = fx_rate
            # transfer.amount_in_local_currency = fx_rate * transfer.amount_in_currency
            # calculated_fee = calculate_commission(transfer.amount_in_currency)
            # transfer.fees_collected = calculated_fee
            # transfer.platform_fees = calculated_fee * Decimal('0.65')
            # transfer.agent_fees = calculated_fee * Decimal('0.35')
            # transfer.amount_expected = transfer.platform_fees + transfer.amount_in_currency
            # transfer.credit += transfer.amount_expected

            totp = pyotp.TOTP(pyotp.random_base32())
            otp = totp.now()

            token = str(otp) + str(request.user.username)
            send_sms(request.user.agent.phone1, 'transaction otp is ' + str(otp))
            cache.set(token, transfer, 180)
            return HttpResponseRedirect(reverse('otp_view'))

            pass
        else:
            schedules = Schedule.objects.filter(mto=request.user.agent.mto)
            context_data = {
                'schedules': schedules,
                'agent': request.user.agent,
                'page_name': page_name,
                'form': transfer_form,
            }

            return render(request, template_name, context_data)

            pass
        pass
    pass


@wrap_try_block_decorator
@transaction.atomic
@user_passes_test(permitted_agent, login_url='/login')
def otp_view(request, template_name='agent_portal/otp.html', page_name='otp'):
    if request.method == 'GET':
        form = OtpForm()
        return render(request, template_name, {
            'agent': request.user.agent,
            'form': form,
            'error': get_flash(request, 'error'),
            'page_name': page_name,
            'schedules': Schedule.objects.filter(mto=request.user.agent.mto),
        })
        pass
    else:
        form = OtpForm(request.POST)

        if form.is_valid():
            otp = form.cleaned_data.get('otp')
            token = otp + request.user.username
            transfer = cache.get(token)

            if transfer:
                transfer.otp = otp
                bank = BankController()
                status, result = bank.transfer_fund(str(otp), str(transfer.beneficiary_bank_name),
                                                    str(transfer.beneficiary_bank_number),
                                                    str(transfer.beneficiary_bank_full_name), str(transfer.amount_in_local_currency),
                                                    str(transfer.narration), "N")

                if status:
                    transfer.status = 's'
                    agent = request.user.agent
                    total_transfer_amount_and_fees_in_currency = transfer.amount_in_currency + transfer.fees_collected
                    agent.account_balance -= total_transfer_amount_and_fees_in_currency
                    agent.save()
                    transfer.save()
                    #send message to the beneficiary and the sender
                    senders_message = str(int(transfer.amount_in_currency)) +'.00' + agent.country.currency.abbreviation + ' sent to ' + transfer.beneficiary_bank_full_name +\
                                      '. Tracking no is '+str(transfer.id)



                    send_sms(transfer.senders_mobile, senders_message)


                    return HttpResponseRedirect(reverse('transfer_invoice_view', args=(transfer.id,)))
                    pass
                else:
                    set_flash(request, 'error', feedback_messages.get('failed_transfer'))
                    ErrorLog.objects.create(status=str(status), error_code=str(result))
                    return HttpResponseRedirect(reverse('dashboard'))
                    pass

            else:
                set_flash(request, 'error', feedback_messages.get('incorrect_otp'))
                HttpResponseRedirect(reverse('otp_view'))

            pass
        else:
            return render(request, template_name, {
                'agent': request.user.agent,
                'form': form,
                'error': get_flash(request, 'error'),
                'page_name': page_name,
                'schedules': Schedule.objects.filter(mto=request.user.agent.mto),
            })
            pass


@wrap_try_block_decorator
@require_GET
@user_passes_test(permitted_agent)
def transfer_invoice_view(request, iden='', template_name='agent_portal/transfer-invoice.html', page_name='transfer invoice'):
    try:
        transfer_detail = Transfer.objects.get(id=int(iden))

        context_data = {
            'agent': request.user.agent,
            'page_name': page_name,
            'transfer': transfer_detail,
            'schedules': Schedule.objects.filter(mto=request.user.agent.mto),
            }
        return render(request, template_name, context_data)
    except ObjectDoesNotExist:
        set_flash(request, 'error', feedback_messages.get('transfer_not_found'))
        return HttpResponseRedirect('dashboard')
        pass


@wrap_try_block_decorator
@user_passes_test(permitted_agent, login_url='/login')
def request_fund_view(request, template_name='agent_portal/add-fund.html', page_name='request fund'):
    if request.method == 'GET':
        form = FundForm()
        return render(request, template_name, {
            'page_name': page_name,
            'form': form,
            'agent': request.user.agent,
            'schedules': Schedule.objects.filter(mto=request.user.agent.mto),
        })
    else:
        form = FundForm(request.POST)

        if form.is_valid():
            fund = form.save(commit=False)
            fund.requested_by = request.user.agent
            fund.status = 'p'
            fund.currency = request.user.agent.country.currency.name
            fund.save()

            set_flash(request, 'message', feedback_messages.get('successful_fund_request'))
            return HttpResponseRedirect(reverse('dashboard'))
        else:
            context_data = {
                'page_name': page_name,
                'form': form,
                'agent': request.user.agent,
                'schedules': Schedule.objects.filter(mto=request.user.agent.mto),
            }
            return render(request, template_name, context_data)
        pass


@wrap_try_block_decorator
@require_GET
@user_passes_test(permitted_agent, login_url='/login')
def transaction_view(request, type_='', template_name='agent_portal/funds.html', page_name='transactions'):

    if type_ == '':
        return HttpResponseRedirect(reverse('dashboard'))
    elif type_ == 'funds':
        transaction_list = Fund.objects.all().filter(requested_by=request.user.agent)
        sub_page_name = 'funds'
    else:
        transaction_list = Transfer.objects.all().filter(agent=request.user.agent)
        template_name = 'agent_portal/transfers.html'
        sub_page_name = 'transfers'

    paginator = Paginator(transaction_list, 10)

    page = request.GET.get('page')

    try:
        transactions = paginator.page(page)
    except PageNotAnInteger:
        transactions = paginator.page(1)
    except EmptyPage:
        transactions = paginator.page(paginator.num_pages)

    context_data = {
        'agent': request.user.agent,
        'page_name': page_name,
        'transactions': transactions,
        'sub_page_name': sub_page_name,
        'schedules': Schedule.objects.filter(mto=request.user.agent.mto),
    }
    return render(request, template_name, context_data)


@wrap_try_block_decorator
def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('home'))


def find_agents_view(request, template_name='agent_portal/find-agent.html', page_name='agents finder'):
    if request.method == 'GET':
        countries = Country.objects.all()
        agents = None

        if request.GET.get('country'):
            country_name = request.GET.get('country')
            agents = Agent.objects.all().filter(country=Country.objects.get(name=country_name))

        return render(request, template_name, {
            'page_name': page_name,
            'countries': countries,
            'agents': agents,
        })
        pass
    else:
        pass
    pass


def rate_view(request, template_name='agent_portal/home.html'):
    context = {
        'currencies': Currency.objects.all()
    }

    return render(request, template_name, context)

@wrap_try_block_decorator
@user_passes_test(permitted_agent, login_url='/login')
def reset_password_view(request, template_name="agent_portal/reset-password.html", page_name=""):

    form = ResetPasswordForm()

    if request.method == "POST":
        form = ResetPasswordForm(request.POST)
        if form.is_valid():
            _user = request.user
            _user.set_password(form.cleaned_data['password1'])
            _user.save()
            set_flash(request, 'message', 'Password successfully reset')
            return HttpResponseRedirect(reverse('dashboard'))
        pass


    context = {
        'agent': request.user.agent,
        'page_name': page_name,
        'schedules': Schedule.objects.filter(mto=request.user.agent.mto),
        'form': form,
        'message': get_flash(request, 'message'),
        'error': get_flash(request, 'error'),

    }

    return render(request, template_name, context)





