from django.db import models
from django.contrib.auth.models import User
from decimal import Decimal
from django.db.models import Sum
from django.core.exceptions import ObjectDoesNotExist
from rsp.errors import InvalidAmountException

from datetime import datetime


# Create your models here.
id_type = (
        ('ID', 'IDENTITY CARD'),
        ('IP', 'INTERNATIONAL PASSPORT'),
        ('DL', 'DRIVERS LICENSE'),
    )



class Currency(models.Model):
    """
        Currency supported on our platform for money transfer
    """
    name = models.CharField(max_length=20, unique=True)
    exchange_rate = models.DecimalField(max_digits=65, decimal_places=2)
    minimum_per_transfer = models.DecimalField(max_digits=65, decimal_places=2)
    maximum_per_transfer = models.DecimalField(max_digits=65, decimal_places=2)
    symbol = models.CharField(max_length=2)
    abbreviation = models.CharField(max_length=10)
    selling_margin = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=65)

    def get_selling_price_exchange_rate(self):
        return self.exchange_rate + self.selling_margin

    def __unicode__(self):
        return self.name





class Country(models.Model):
    """
        Countries supported with currency foreign key
    """
    name = models.CharField(max_length=100, unique=True)
    abbreviation = models.CharField(max_length=10)
    currency = models.ForeignKey(Currency)


    def __unicode__(self):
        return self.name


class Mto(models.Model):
    """
    Mto/Aggregator information provided during partnership
    """
    name = models.CharField(max_length=50, unique=True)
    address = models.CharField(max_length=100)
    agent_commission = models.DecimalField(max_digits=65, decimal_places=2, default=Decimal('0'))
    platform_commission = models.DecimalField(max_digits=65, decimal_places=2, default=Decimal('100'))
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-date']


    def __unicode__(self):
        return self.name



class Rate(models.Model):
    rate = models.DecimalField(max_digits=64, decimal_places=2)
    currency = models.ForeignKey(Currency)
    mto = models.ForeignKey(Mto)
    selling_margin = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=65)

    @classmethod
    def get_exchange_rate(cls, agent):
        try:
            rate = cls.objects.get(currency=agent.country.currency, mto=agent.mto)
            exchange_rate = rate.rate
        except ObjectDoesNotExist:
            exchange_rate = agent.country.currency.exchange_rate

        return exchange_rate

class Schedule(models.Model):
    commission_types = (
        ('percentage', 'Percentage'),
        ('fixed', 'Fixed')
    )
    greater_than_equal_to = models.DecimalField(max_digits=64, decimal_places=2)
    less_than_equal_to = models.DecimalField(max_digits=64, decimal_places=2)
    commission_type = models.CharField(max_length=20, choices=commission_types)
    commission = models.DecimalField(max_digits=64, decimal_places=2)
    mto = models.ForeignKey(Mto)
    date = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return str(self.greater_than_equal_to) + str(self.less_than_equal_to)


class Agent(models.Model):
    """
        Registered agents information
    """

    user = models.OneToOneField(User)
    country = models.ForeignKey(Country)
    city = models.CharField(max_length=20)
    address = models.CharField(max_length=50)
    phone1 = models.CharField(max_length=20)
    phone2 = models.CharField(max_length=20, blank=True)
    social_security_number = models.CharField("Social Security Number | NI Number ", max_length=50)
    mto = models.ForeignKey(Mto)
    identification_type = models.CharField(max_length=20, choices=id_type)
    identification_number = models.CharField(max_length=50)
    business_name = models.CharField(max_length=50)
    position = models.CharField(max_length=50)
    date_of_birth = models.CharField(max_length=10)
    account_balance = models.DecimalField(max_digits=65, decimal_places=2, default=Decimal('0.0'))
    super_agent = models.ForeignKey('self', null=True, blank=True,)
    is_super_agent = models.BooleanField(default=False)
    credit = models.DecimalField(max_digits=65, decimal_places=2, default=Decimal('0.0'))



    def __unicode__(self):
        return self.user.username

    def is_secret_answer_correct(self, question=None, answer=None):
        try:
            Secret.objects.get(agent=self, question=question, answer=answer)
            return True
        except Exception:
            return False

    def latest_transfers(self, size=5):
        query_set = Transfer.objects.filter(agent=self)[:size]
        return query_set

    def latest_funds(self, size=5):
        query_set = Fund.objects.filter(requested_by=self)[:size]
        return query_set

class Payment(models.Model):
    amount = models.DecimalField(max_digits=65, decimal_places=2)
    collected_on = models.DateTimeField(auto_now_add=True)
    agent = models.ForeignKey(Agent)

    class Meta:
        ordering = ['-collected_on']




secret_questions = ('Where were you born?', 'Who is your favourite celebrity?',
                    'What is the name of your favourite childhood friend?', 'In what city or town was your first job?')
class Secret(models.Model):
    """
    Secret question and md5 hashed answers for registered agents
    """
    agent = models.ForeignKey(Agent)
    question = models.CharField(max_length=100)
    answer = models.CharField(max_length=100)

    def __unicode__(self):
        return self.question




class Fund(models.Model):
    """
        Requested funds by agents.
    """
    fund_types = (
        ('bank deposit', 'Bank Deposit'),
        ('mto/aggregator deposit', 'MTO/AGGREGATOR DEPOSIT'),
        ('credit', 'CREDIT'),
    )
    fund_status = (('r', 'rejected'), ('p', 'pending'), ('f', 'funded'))
    requested_by = models.ForeignKey(Agent)
    funded_by = models.ForeignKey(User, null=True)
    amount = models.DecimalField(max_digits=65, decimal_places=2)
    type = models.CharField(max_length=100, choices=fund_types)
    currency = models.CharField(max_length=100)
    requested_on = models.DateTimeField(auto_now_add=True)
    funded_on = models.DateTimeField(null=True, blank=True)
    status = models.CharField(max_length=10, choices=fund_status)

    class Meta:
        ordering = ['-requested_on']

class ErrorLog(models.Model):
    status = models.CharField(max_length=10)
    error_code = models.CharField(max_length=10)

def calculate_commission(amount, agent):
    mto_schedules = Schedule.objects.filter(mto=agent.mto)

    for schedule in mto_schedules:
        if amount >= schedule.greater_than_equal_to and amount <= schedule.less_than_equal_to:
            if schedule.commission_type == 'percentage':
                return schedule.commission * amount
            else:
                return schedule.commission

    raise InvalidAmountException('Amount does not have schedule')

class Transfer(models.Model):
    """
        Transfer made by registered agents.
    """
    transfer_status = (
        ('s', 'success'),
        ('f', 'failed')
    )
    supported_banks = (
        ('044', 'ACCESS BANK'),
        ('023', 'CITI Bank'),
        ('063', 'DIAMOND BANK'),
        ('401', 'ASO SAVINGS'),
        ('050', 'ECOBANK'),
        ('084', 'ENTERPRISE BANK'),
        ('040', 'EQUITORIAL TRUST BANK LIMITED'),
        ('070', 'FIDELITY BANK'),
        ('011', 'FIRST BANK OF NIGERIA'),
        ('214', 'FIRST CITY MONUMENT BANK'),
        ('085', 'FIRST INLAND BANK'),
        ('058', 'GUARANTY TRUST BANK'),
        ('030', 'HERITAGE BANK'),
        ('069', 'INTERCONTINENTAL BANK'),
        ('082', 'KEYSTONE BANK'),
        ('014', 'MAINSTREET BANK'),
        ('056', 'OCEANIC BANK'),
        ('076', 'SKYE BANK'),
        ('221', 'STANBIC IBTC BANK'),
        ('068', 'STANDARD CHARTERED BANK NIGERIA LTD'),
        ('232', 'STERLING BANK'),
        ('032', 'UNION BANK'),
        ('033', 'UNITED BANK FOR AFRICA'),
        ('215', 'UNITY BANK'),
        ('035', 'WEMA BANK'),
        ('057', 'ZENITH BANK'),
    )

    settlement_status_choices = (
        ('pending', 'Pending'),
        ('settled', 'settled')
    )
    agent = models.ForeignKey(Agent)
    amount_in_currency = models.DecimalField(max_digits=65, decimal_places=2)
    amount_in_local_currency = models.DecimalField(max_digits=65, decimal_places=2)
    fees_collected = models.DecimalField(max_digits=65, decimal_places=2)
    currency = models.CharField(max_length=100)
    exchange_rate = models.DecimalField(max_digits=65, decimal_places=2)
    narration = models.CharField(max_length=100)
    time = models.DateTimeField(auto_now_add=True)
    otp = models.CharField(max_length=100)
    status = models.CharField(max_length=50, choices=transfer_status)
    senders_name = models.CharField(max_length=100, verbose_name='Senders Fullname')
    senders_id_type = models.CharField(max_length=20, choices=id_type)
    senders_id_number = models.CharField(max_length=50)
    senders_mobile = models.CharField(max_length=20)
    senders_address = models.CharField(max_length=50)
    senders_email = models.CharField(max_length=100, blank=True)
    beneficiary_bank_full_name = models.CharField(max_length=50)
    beneficiary_bank_number = models.CharField(max_length=15)
    beneficiary_mobile_number = models.CharField(max_length=20)
    beneficiary_bank_name = models.CharField(max_length=50, choices=supported_banks)
    platform_fees = models.DecimalField(max_digits=65, decimal_places=2, null=True, blank=True, default=Decimal('0'))
    amount_expected = models.DecimalField(max_digits=65, decimal_places=2, null=True, blank=True, default=Decimal('0'))
    agent_fees = models.DecimalField(max_digits=65, decimal_places=2, default=Decimal('0'), null=True, blank=True)
    settlement_status = models.CharField(max_length=50, default='pending', choices=settlement_status_choices)
    settlement_date = models.DateField(null=True, blank=True)
    wema_fee = models.DecimalField(max_digits=65, default=Decimal('300'), decimal_places=2, null=True, blank=True)

    class Meta:
        ordering = ['-time']

    @classmethod
    def total_naira_transfered_today(cls):
        total = Decimal('0')
        try:
            total_sum = cls.objects.filter(time=datetime.now().date()).aggregate(Sum('amount_in_local_currency'))
            total = total_sum.amount_in_local_currency__sum
        except Exception:
            pass

        return total

    def total_principal_and_fee(self):
        return self.amount_in_currency + self.platform_fees

    def prepare_transfer(self, agent):
        self.currency = agent.country.currency.name
        self.agent = agent

        self.exchange_rate = Rate.get_exchange_rate(agent)
        self.amount_in_local_currency = self.exchange_rate * self.amount_in_currency

        calculated_commision = calculate_commission(self.amount_in_currency, agent)
        self.fees_collected = calculated_commision
        self.platform_fees = self.fees_collected * agent.mto.platform_commission
        self.agent_fees = self.fees_collected * agent.mto.agent_commission
        self.amount_expected = self.amount_in_currency + self.platform_fees
        self.agent.credit += self.amount_expected

        pass


    def __unicode__(self):
        return self.otp



class Setting(models.Model):
    prop = models.CharField(max_length=50)
    value = models.TextField()












