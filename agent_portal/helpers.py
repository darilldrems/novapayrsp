from .strings import flash_prefix, feedback_messages
from decimal import Decimal


from django.core.exceptions import ObjectDoesNotExist



from twilio.rest import TwilioRestClient


def set_flash(request, name, value):
    request.session[flash_prefix+name] = value


def get_flash(request, name):
    flash_message = request.session.get(flash_prefix+name)
    request.session[flash_prefix+name] = ''
    return flash_message


def permitted_agent(user):
    try:
        if user.is_active and user.is_authenticated():
            user.agent
            return True
        else:
            return False
    except ObjectDoesNotExist:
        return False

def send_sms(to, message):
    try:
        account_sid = 'AC3d2767080050489684731450fc43bb97'
        auth_token = '557a71399e0159fbc5f88ad3efc289e7'
        twilio_number = '5403524219'

        client = TwilioRestClient(account_sid, auth_token)

        client.messages.create(
            from_=twilio_number,
            to=to,
            body=message
        )
    except Exception:
        pass

def calculate_commission(amount):
    calculated_fee = Decimal('0')

    if 50 <= amount <= 100:
        calculated_fee = Decimal('3')
    if 100 <= amount <= 400:
        calculated_fee = Decimal('0.03') * amount
    elif 400 < amount <= 900:
        calculated_fee = Decimal('0.02') * amount

    elif 900 < amount <= 2000:
        calculated_fee = Decimal('25')
    return calculated_fee





def wrap_try_block_decorator(method):

    def wrap(request, *args, **kwargs):
        from django.http.response import HttpResponseRedirect
        from django.core.urlresolvers import reverse
        import logging

        logger = logging.getLogger(__name__)

        try:
            return method(request, *args, **kwargs)
        except Exception:
            # logger.exception('Exception Caught:')
            set_flash(request, 'error', feedback_messages.get('something_went_wrong'))
            return HttpResponseRedirect(reverse('dashboard'))
    return wrap
