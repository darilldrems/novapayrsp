from django import forms
from django.contrib.auth.models import User




from .models import secret_questions, Agent, Fund, Transfer, Mto, Schedule, Country, Currency, Rate

import random

class ResetPasswordForm(forms.Form):
    password1 = forms.CharField(min_length=8, max_length=40, widget=forms.PasswordInput, label="Password")
    password2 = forms.CharField(min_length=8, max_length=40, widget=forms.PasswordInput, label="Confirm password")

    error_messages = {
        'password_mismatch': "The two password fields didn't match.",
    }

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def save(self, commit=True):
        user = super(UserForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user
    pass

class UserForm(forms.ModelForm):

    error_messages = {
        'duplicate_username': "A user with that username already exists.",
        'password_mismatch': "The two password fields didn't match.",
    }

    username = forms.CharField(max_length=20, label="Username", help_text='')
    password1 = forms.CharField(min_length=8, max_length=20, widget=forms.PasswordInput, label="Password")
    password2 = forms.CharField(min_length=8, max_length=20, widget=forms.PasswordInput, label="Password confirmation", help_text="Enter the same password as above")

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2')
        # exclude = ('is_staff', 'is_active', 'date_joined', 'password', 'last_login')

    def clean_username(self):
        # Since User.username is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        username = self.cleaned_data["username"]
        try:
            User._default_manager.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(
            self.error_messages['duplicate_username'],
            code='duplicate_username',
        )

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def save(self, commit=True):
        user = super(UserForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user
    pass


class CountryForm(forms.ModelForm):
    class Meta:
        model = Country

class RateForm(forms.ModelForm):
    class Meta:
        model = Rate

class AgentForm(forms.ModelForm):
    super_agent = forms.ModelChoiceField(queryset=Agent.objects.exclude(is_super_agent=False), required=False)
    class Meta:
        model = Agent
        fields = ('country', 'city', 'address', 'phone1', 'social_security_number', 'mto',
                  'identification_type', 'identification_number', 'business_name', 'position', 'date_of_birth','super_agent',)
    pass

class ScheduleForm(forms.ModelForm):
    class Meta:
        model = Schedule

class CurrencyForm(forms.ModelForm):
    class Meta:
        model = Currency

class SecretForm(forms.Form):
    question_1 = forms.CharField(max_length=100, label=secret_questions[0])
    question_2 = forms.CharField(max_length=100, label=secret_questions[1])
    question_3 = forms.CharField(max_length=100, label=secret_questions[2])
    question_4 = forms.CharField(max_length=100, label=secret_questions[3])

    pass

class SettlementLoginForm(forms.Form):
    username = forms.CharField(max_length=100)
    password = forms.CharField(max_length=100)

class MtoForm(forms.ModelForm):
    class Meta:
        model = Mto

class SecretLoginForm(forms.Form):
    question = forms.CharField(max_length=100, label=secret_questions[random.randrange(0, 3)])

class FundForm(forms.ModelForm):
    class Meta:
        model = Fund
        fields = ('type', 'amount')
    pass


class TransferForm(forms.ModelForm):
    class Meta:
        model = Transfer
        fields = ('amount_in_currency', 'narration', 'senders_name',
                  'senders_id_type', 'senders_id_number', 'senders_mobile', 'senders_address', 'senders_email',
                  'beneficiary_bank_name', 'beneficiary_bank_number', 'beneficiary_bank_full_name',
                  'beneficiary_mobile_number')

class OtpForm(forms.Form):
    otp = forms.CharField(max_length=100, label='Transfer otp')

class LoginForm(forms.Form):
    username = forms.CharField(max_length=50, label='Username')
    password = forms.CharField(widget=forms.PasswordInput)

