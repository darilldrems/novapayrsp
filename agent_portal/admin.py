from django.contrib import admin
from agent_portal.models import Country, Mto, Currency, Agent, Secret, Transfer, ErrorLog, Rate, Schedule
# Register your models here.

class CountryAdmin(admin.ModelAdmin):
    pass

class MtoAdmin(admin.ModelAdmin):
    pass

class ScheduleAdmin(admin.ModelAdmin):
    list_display = ('greater_than_equal_to', 'less_than_equal_to')

class AgentAdmin(admin.ModelAdmin):
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'super_agent':
            kwargs['queryset'] = Agent.objects.filter(is_super_agent=True)

        return db_field.formfield(**kwargs)
    # limit_choices_to = {'super_agent': Agent.objects.exclude(is_super_agent=False)}
    pass


class SecretAdmin(admin.ModelAdmin):
    list_display = ('agent', 'question', 'answer')


class TransferAdmin(admin.ModelAdmin):
    list_display = ('agent', 'amount_in_currency', 'amount_in_local_currency', 'fees_collected', 'currency', 'exchange_rate', 'otp', 'status', 'senders_name', 'beneficiary_bank_full_name', 'beneficiary_bank_number', 'beneficiary_bank_name', 'time')


admin.site.register(Country, CountryAdmin)
admin.site.register(Mto, MtoAdmin)
admin.site.register(Currency)
admin.site.register(Agent, AgentAdmin)
admin.site.register(Secret, SecretAdmin)
admin.site.register(Transfer, TransferAdmin)
admin.site.register(Schedule, ScheduleAdmin)
admin.site.register(ErrorLog)
admin.site.register(Rate)