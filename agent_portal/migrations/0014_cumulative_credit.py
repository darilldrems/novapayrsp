# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models
from decimal import Decimal

class Migration(DataMigration):

    def forwards(self, orm):
        "Write your forwards methods here."
        # Note: Don't use "from appname.models import ModelName". 
        # Use orm.ModelName to refer to models in this application,
        # and orm['appname.ModelName'] for models in other applications.
        for a in orm.Agent.objects.all():
            for t in orm.Transfer.objects.filter(agent=a):
                a.credit += t.amount_expected
            a.save()

    def backwards(self, orm):
        "Write your backwards methods here."
        for a in orm.Agent.objects.all():
            a.credit = Decimal('0')
            a.save()

    models = {
        u'agent_portal.agent': {
            'Meta': {'object_name': 'Agent'},
            'account_balance': ('django.db.models.fields.DecimalField', [], {'default': "'0.0'", 'max_digits': '65', 'decimal_places': '2'}),
            'address': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'business_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['agent_portal.Country']"}),
            'credit': ('django.db.models.fields.DecimalField', [], {'default': "'0.0'", 'max_digits': '65', 'decimal_places': '2'}),
            'date_of_birth': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identification_number': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'identification_type': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'is_super_agent': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'mto': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['agent_portal.Mto']"}),
            'payments': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['agent_portal.Payment']", 'null': 'True', 'blank': 'True'}),
            'phone1': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'phone2': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'position': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'social_security_number': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'super_agent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['agent_portal.Agent']", 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'})
        },
        u'agent_portal.country': {
            'Meta': {'object_name': 'Country'},
            'abbreviation': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['agent_portal.Currency']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'agent_portal.currency': {
            'Meta': {'object_name': 'Currency'},
            'abbreviation': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'exchange_rate': ('django.db.models.fields.DecimalField', [], {'max_digits': '65', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'maximum_per_transfer': ('django.db.models.fields.DecimalField', [], {'max_digits': '65', 'decimal_places': '2'}),
            'minimum_per_transfer': ('django.db.models.fields.DecimalField', [], {'max_digits': '65', 'decimal_places': '2'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'symbol': ('django.db.models.fields.CharField', [], {'max_length': '2'})
        },
        u'agent_portal.errorlog': {
            'Meta': {'object_name': 'ErrorLog'},
            'error_code': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        u'agent_portal.fund': {
            'Meta': {'ordering': "['-requested_on']", 'object_name': 'Fund'},
            'amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '65', 'decimal_places': '2'}),
            'currency': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'funded_by': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'null': 'True'}),
            'funded_on': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'requested_by': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['agent_portal.Agent']"}),
            'requested_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'agent_portal.mto': {
            'Meta': {'object_name': 'Mto'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'agent_portal.payment': {
            'Meta': {'object_name': 'Payment'},
            'amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '65', 'decimal_places': '2'}),
            'collected_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'agent_portal.secret': {
            'Meta': {'object_name': 'Secret'},
            'agent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['agent_portal.Agent']"}),
            'answer': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'agent_portal.setting': {
            'Meta': {'object_name': 'Setting'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'prop': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'value': ('django.db.models.fields.TextField', [], {})
        },
        u'agent_portal.transfer': {
            'Meta': {'ordering': "['-time']", 'object_name': 'Transfer'},
            'agent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['agent_portal.Agent']"}),
            'agent_fees': ('django.db.models.fields.DecimalField', [], {'default': "'0'", 'null': 'True', 'max_digits': '65', 'decimal_places': '2', 'blank': 'True'}),
            'amount_expected': ('django.db.models.fields.DecimalField', [], {'default': "'0'", 'null': 'True', 'max_digits': '65', 'decimal_places': '2', 'blank': 'True'}),
            'amount_in_currency': ('django.db.models.fields.DecimalField', [], {'max_digits': '65', 'decimal_places': '2'}),
            'amount_in_local_currency': ('django.db.models.fields.DecimalField', [], {'max_digits': '65', 'decimal_places': '2'}),
            'beneficiary_bank_full_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'beneficiary_bank_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'beneficiary_bank_number': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'beneficiary_mobile_number': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'currency': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'exchange_rate': ('django.db.models.fields.DecimalField', [], {'max_digits': '65', 'decimal_places': '2'}),
            'fees_collected': ('django.db.models.fields.DecimalField', [], {'max_digits': '65', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'narration': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'otp': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'platform_fees': ('django.db.models.fields.DecimalField', [], {'default': "'0'", 'null': 'True', 'max_digits': '65', 'decimal_places': '2', 'blank': 'True'}),
            'senders_address': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'senders_email': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'senders_id_number': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'senders_id_type': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'senders_mobile': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'senders_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'settlement_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'settlement_status': ('django.db.models.fields.CharField', [], {'default': "'pending'", 'max_length': '50'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'wema_fee': ('django.db.models.fields.DecimalField', [], {'default': "'300'", 'null': 'True', 'max_digits': '65', 'decimal_places': '2', 'blank': 'True'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['agent_portal']
    symmetrical = True
