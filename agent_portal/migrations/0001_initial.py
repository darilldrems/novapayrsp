# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Currency'
        db.create_table(u'agent_portal_currency', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('exchange_rate', self.gf('django.db.models.fields.DecimalField')(max_digits=65, decimal_places=2)),
            ('minimum_per_transfer', self.gf('django.db.models.fields.DecimalField')(max_digits=65, decimal_places=2)),
            ('maximum_per_transfer', self.gf('django.db.models.fields.DecimalField')(max_digits=65, decimal_places=2)),
            ('symbol', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('abbreviation', self.gf('django.db.models.fields.CharField')(max_length=10)),
        ))
        db.send_create_signal(u'agent_portal', ['Currency'])

        # Adding model 'Country'
        db.create_table(u'agent_portal_country', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('abbreviation', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('currency', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['agent_portal.Currency'])),
        ))
        db.send_create_signal(u'agent_portal', ['Country'])

        # Adding model 'Mto'
        db.create_table(u'agent_portal_mto', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'agent_portal', ['Mto'])

        # Adding model 'Agent'
        db.create_table(u'agent_portal_agent', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True)),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['agent_portal.Country'])),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('phone1', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('phone2', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('social_security_number', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('mto', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['agent_portal.Mto'])),
            ('identification_type', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('identification_number', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('business_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('position', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('date_of_birth', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('account_balance', self.gf('django.db.models.fields.DecimalField')(default='0.0', max_digits=65, decimal_places=2)),
        ))
        db.send_create_signal(u'agent_portal', ['Agent'])

        # Adding model 'Secret'
        db.create_table(u'agent_portal_secret', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('agent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['agent_portal.Agent'])),
            ('question', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('answer', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'agent_portal', ['Secret'])

        # Adding model 'Fund'
        db.create_table(u'agent_portal_fund', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('requested_by', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['agent_portal.Agent'])),
            ('funded_by', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], null=True)),
            ('amount', self.gf('django.db.models.fields.DecimalField')(max_digits=65, decimal_places=2)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('currency', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('requested_on', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('funded_on', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=10)),
        ))
        db.send_create_signal(u'agent_portal', ['Fund'])

        # Adding model 'ErrorLog'
        db.create_table(u'agent_portal_errorlog', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('error_code', self.gf('django.db.models.fields.CharField')(max_length=10)),
        ))
        db.send_create_signal(u'agent_portal', ['ErrorLog'])

        # Adding model 'Transfer'
        db.create_table(u'agent_portal_transfer', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('agent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['agent_portal.Agent'])),
            ('amount_in_currency', self.gf('django.db.models.fields.DecimalField')(max_digits=65, decimal_places=2)),
            ('amount_in_local_currency', self.gf('django.db.models.fields.DecimalField')(max_digits=65, decimal_places=2)),
            ('fees_collected', self.gf('django.db.models.fields.DecimalField')(max_digits=65, decimal_places=2)),
            ('currency', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('exchange_rate', self.gf('django.db.models.fields.DecimalField')(max_digits=65, decimal_places=2)),
            ('narration', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('otp', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('senders_name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('senders_id_type', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('senders_id_number', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('senders_mobile', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('senders_address', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('senders_email', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('beneficiary_bank_full_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('beneficiary_bank_number', self.gf('django.db.models.fields.CharField')(max_length=15)),
            ('beneficiary_mobile_number', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('beneficiary_bank_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'agent_portal', ['Transfer'])

        # Adding model 'Setting'
        db.create_table(u'agent_portal_setting', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('prop', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('value', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'agent_portal', ['Setting'])


    def backwards(self, orm):
        # Deleting model 'Currency'
        db.delete_table(u'agent_portal_currency')

        # Deleting model 'Country'
        db.delete_table(u'agent_portal_country')

        # Deleting model 'Mto'
        db.delete_table(u'agent_portal_mto')

        # Deleting model 'Agent'
        db.delete_table(u'agent_portal_agent')

        # Deleting model 'Secret'
        db.delete_table(u'agent_portal_secret')

        # Deleting model 'Fund'
        db.delete_table(u'agent_portal_fund')

        # Deleting model 'ErrorLog'
        db.delete_table(u'agent_portal_errorlog')

        # Deleting model 'Transfer'
        db.delete_table(u'agent_portal_transfer')

        # Deleting model 'Setting'
        db.delete_table(u'agent_portal_setting')


    models = {
        u'agent_portal.agent': {
            'Meta': {'object_name': 'Agent'},
            'account_balance': ('django.db.models.fields.DecimalField', [], {'default': "'0.0'", 'max_digits': '65', 'decimal_places': '2'}),
            'address': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'business_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['agent_portal.Country']"}),
            'date_of_birth': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identification_number': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'identification_type': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'mto': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['agent_portal.Mto']"}),
            'phone1': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'phone2': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'position': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'social_security_number': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'})
        },
        u'agent_portal.country': {
            'Meta': {'object_name': 'Country'},
            'abbreviation': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['agent_portal.Currency']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'agent_portal.currency': {
            'Meta': {'object_name': 'Currency'},
            'abbreviation': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'exchange_rate': ('django.db.models.fields.DecimalField', [], {'max_digits': '65', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'maximum_per_transfer': ('django.db.models.fields.DecimalField', [], {'max_digits': '65', 'decimal_places': '2'}),
            'minimum_per_transfer': ('django.db.models.fields.DecimalField', [], {'max_digits': '65', 'decimal_places': '2'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'symbol': ('django.db.models.fields.CharField', [], {'max_length': '2'})
        },
        u'agent_portal.errorlog': {
            'Meta': {'object_name': 'ErrorLog'},
            'error_code': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        u'agent_portal.fund': {
            'Meta': {'object_name': 'Fund'},
            'amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '65', 'decimal_places': '2'}),
            'currency': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'funded_by': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'null': 'True'}),
            'funded_on': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'requested_by': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['agent_portal.Agent']"}),
            'requested_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'agent_portal.mto': {
            'Meta': {'object_name': 'Mto'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'agent_portal.secret': {
            'Meta': {'object_name': 'Secret'},
            'agent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['agent_portal.Agent']"}),
            'answer': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'agent_portal.setting': {
            'Meta': {'object_name': 'Setting'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'prop': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'value': ('django.db.models.fields.TextField', [], {})
        },
        u'agent_portal.transfer': {
            'Meta': {'ordering': "['-time']", 'object_name': 'Transfer'},
            'agent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['agent_portal.Agent']"}),
            'amount_in_currency': ('django.db.models.fields.DecimalField', [], {'max_digits': '65', 'decimal_places': '2'}),
            'amount_in_local_currency': ('django.db.models.fields.DecimalField', [], {'max_digits': '65', 'decimal_places': '2'}),
            'beneficiary_bank_full_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'beneficiary_bank_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'beneficiary_bank_number': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'beneficiary_mobile_number': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'currency': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'exchange_rate': ('django.db.models.fields.DecimalField', [], {'max_digits': '65', 'decimal_places': '2'}),
            'fees_collected': ('django.db.models.fields.DecimalField', [], {'max_digits': '65', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'narration': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'otp': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'senders_address': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'senders_email': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'senders_id_number': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'senders_id_type': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'senders_mobile': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'senders_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['agent_portal']