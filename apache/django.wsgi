import os, sys
sys.path.append('/var/www/django-projects/novapayrsp')
os.environ['DJANGO_SETTINGS_MODULE'] = 'novapay.settings'

import django.core.handlers.wsgi

application = django.core.handlers.wsgi.WSGIHandler()