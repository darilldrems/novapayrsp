from django.db import models
from django.core.exceptions import ObjectDoesNotExist

from helpers import build_signatures


from errors import AmountAboveMaximumException, AmountBelowMinimumException, OperationException

from errorcodes import ErrorMessages

class Mto(models.Model):
    dev_mode = (
        ('1', 'testing'),
        ('2', 'live'),
        ('3', 'default'),
    )
    vendor_id = models.CharField(max_length=100)
    name = models.CharField(max_length=50)
    created_on = models.DateField(auto_now_add=True)
    ip_address = models.CharField(max_length=20)
    token = models.CharField(max_length=200)
    dev = models.CharField(max_length=10, choices=dev_mode)

    def __unicode__(self):
        return self.name

    @classmethod
    def is_sig_correct(cls, vendor, mtcn, signature):
        try:
            mto = cls.objects.get(vendor_id=vendor)



            original_signature = build_signatures(vendor, mto.token, mtcn)

            if signature == original_signature:
                return True
        except ObjectDoesNotExist:
            return False
        pass

    @classmethod
    def is_mto_ip_correct(cls, vendor, ip):
        try:
            Mto.objects.get(vendor_id=vendor, ip_address=ip)
            return True
        except ObjectDoesNotExist:
            return False
        pass

class Currency(models.Model):
    name = models.CharField(max_length=50)
    exchange_rate = models.DecimalField(max_digits=65, decimal_places=2)
    minimum = models.DecimalField(max_digits=65, decimal_places=2, default=0.00)
    maximum = models.DecimalField(max_digits=65, decimal_places=2, default=0.00)
    # created_on = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.name


    @classmethod
    def not_below_minimum_and_not_above_maximum(cls, currency, amount_in_float):
        try:
            cur = cls.objects.get(name=currency)

            if cur.minimum > amount_in_float:
                raise AmountBelowMinimumException(ErrorMessages.amount_below_minimum)

            if cur.maximum < amount_in_float:
                raise AmountAboveMaximumException(ErrorMessages.amount_above_maximum)
        except cls.DoesNotExist:
            raise OperationException(ErrorMessages.currency_not_supported)
            pass


class Transfer(models.Model):
    supported_banks = (
        ('044', 'ACCESS BANK'),
        ('023', 'CITI Bank'),
        ('063', 'DIAMOND BANK'),
        ('401', 'ASO SAVINGS'),
        ('050', 'ECOBANK'),
        ('084', 'ENTERPRISE BANK'),
        ('040', 'EQUITORIAL TRUST BANK LIMITED'),
        ('070', 'FIDELITY BANK'),
        ('011', 'FIRST BANK OF NIGERIA'),
        ('214', 'FIRST CITY MONUMENT BANK'),
        ('085', 'FIRST INLAND BANK'),
        ('058', 'GUARANTY TRUST BANK'),
        ('030', 'HERITAGE BANK'),
        ('069', 'INTERCONTINENTAL BANK'),
        ('082', 'KEYSTONE BANK'),
        ('014', 'MAINSTREET BANK'),
        ('056', 'OCEANIC BANK'),
        ('076', 'SKYE BANK'),
        ('221', 'STANBIC IBTC BANK'),
        ('068', 'STANDARD CHARTERED BANK NIGERIA LTD'),
        ('232', 'STERLING BANK'),
        ('032', 'UNION BANK'),
        ('033', 'UNITED BANK FOR AFRICA'),
        ('215', 'UNITY BANK'),
        ('035', 'WEMA BANK'),
        ('057', 'ZENITH BANK'),
    )

    identification_types = (
        ('IP', 'International Passport'),
        ('DL', 'Drivers License'),
    )
    transaction_status = (('1', 'Successful'), ('0', 'Failed'))

    mto = models.ForeignKey(Mto)
    mtcn = models.CharField(max_length=100, editable=False)
    beneficiary_bank = models.CharField(max_length=10, choices=supported_banks, editable=False)
    beneficiary_bank_account_number = models.CharField(max_length=20, editable=False)
    beneficiary_bank_account_name = models.CharField(max_length=100, editable=False)
    narration = models.CharField(max_length=100)
    senders_fullname = models.CharField(max_length=50, editable=False)
    senders_id_type = models.CharField(max_length=50, choices=identification_types, editable=False)
    senders_id_number = models.CharField(max_length=50, editable=False)
    senders_country = models.CharField(max_length=50, editable=False)
    senders_state = models.CharField(max_length=50, editable=False)
    senders_mobile = models.CharField(max_length=50, editable=False)
    amount = models.DecimalField(max_digits=65, decimal_places=2, editable=False)
    currency = models.CharField(max_length=50, editable=False)
    exchange_rate = models.DecimalField(max_digits=65, decimal_places=2, editable=False)
    amount_in_naira = models.DecimalField(max_digits=65, decimal_places=2, editable=False)
    amount_in_naira_after_fees = models.DecimalField(max_digits=65, decimal_places=2, editable=False)
    fees = models.DecimalField(max_digits=65, decimal_places=2, default=0.00, editable=False)
    status = models.CharField(max_length=10, choices=transaction_status, editable=False)
    time = models.DateTimeField(auto_now_add=True)


    def __unicode__(self):
        return self.mtcn

    @classmethod
    def mtcn_already_used(cls, _mtcn):
        try:
            mtcn_ = cls.objects.get(mtcn=_mtcn)
            return True
        except ObjectDoesNotExist:
            return False


    @classmethod
    def add_transfer_to_db(cls, **kwargs):
        new_transfer = cls()

        for key, value in kwargs.items():
            if key == 'mto':
                new_transfer.__dict__[key+'_id'] = value.id
            else:
                new_transfer.__dict__[key] = value
            # print key, '==' ,type(value)



        if cls.mtcn_already_used(new_transfer.mtcn):
            raise OperationException(ErrorMessages.mtcn_already_used)
        new_transfer.save()
        return new_transfer.mtcn


class Alert(models.Model):
    error_types = (
        ('001', 'NIBSS'),
        ('002', 'APP')
    )
    type = models.CharField(max_length=5, choices=error_types)
    reason = models.CharField(max_length=100)
    time = models.DateTimeField(auto_now_add=True)

    @classmethod
    def add_alert(cls, **kwargs):
        alert = cls()

        for key, value in kwargs.items():
            alert.__dict__[key] = value

        alert.save()
        return alert.id






