from django.contrib import admin

# Register your models here.
from rsp.models import Mto, Currency, Transfer, Alert


class CurrencyAdmin(admin.ModelAdmin):
    list_display = ('name', 'exchange_rate', 'minimum', 'maximum')


class TransferAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class AlertAdmin(admin.ModelAdmin):
    list_display = ('type', 'reason', 'time')


admin.site.register(Currency, CurrencyAdmin)
admin.site.register(Mto)
admin.site.register(Transfer)
admin.site.register(Alert, AlertAdmin)
