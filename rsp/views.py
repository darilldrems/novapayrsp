#from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.http import require_GET, require_POST
from django.core.exceptions import ObjectDoesNotExist

from bankcontroller import BankController

import base64
import cgi
from decimal import *

from errors import OperationException, FieldException, IpNotAllowedException, AmountBelowMinimumException, AmountAboveMaximumException
from validation import Validation
from bankcodes import bank_codes
from errorcodes import ErrorMessages
from helpers import return_response_text, is_ip_allowed, calculate_deposit_minus_fees, get_set_fees
from bankresultcodes import ResultCodes


from models import Currency, Transfer, Mto, Alert


from ipware.ip import get_real_ip

success_code = '1'
failure_code = '0'


@require_GET
def exchange_rate_view(request, currency=''):
    try:

        # is_ip_allowed(get_real_ip(request))

        if currency == '':
            raise OperationException(ErrorMessages.no_currency_selected)

        selected_currency = Currency.objects.get(name=currency)


        return HttpResponse(return_response_text(success_code, str(selected_currency.exchange_rate)))
    except OperationException, ex:
        return HttpResponse(return_response_text(failure_code, ex.message))
    except Currency.DoesNotExist:
        return HttpResponse(return_response_text(failure_code, ErrorMessages.currency_not_supported))
    except Exception, ex:
        Alert(type='002', reason=ex.message)
        return HttpResponse(return_response_text(failure_code, ErrorMessages.something_went_wrong))
        pass

    pass


@require_GET
def calculate_deposit_view(request, currency='', amount=''):
    try:

        # is_ip_allowed(get_real_ip(request))

        if currency == '':
            raise OperationException(ErrorMessages.no_currency_selected)

        cur = Currency.objects.get(name=currency)

        if not cur:
            raise OperationException(ErrorMessages.currency_not_supported)

        amount_in_decimal = Decimal(amount)

        if cur.minimum > amount_in_decimal:
            raise OperationException(ErrorMessages.amount_below_minimum)

        if cur.maximum < amount_in_decimal:
            raise OperationException(ErrorMessages.amount_above_maximum)

        amount_in_naira_after_fees = calculate_deposit_minus_fees(cur.exchange_rate * amount_in_decimal)

        return HttpResponse(return_response_text(success_code, amount_in_naira_after_fees))
    except ValueError:
        return HttpResponse(return_response_text(failure_code, ErrorMessages.invalid_amount))
        pass
    except OperationException, ex:
        return HttpResponse(return_response_text(failure_code, ex.message))
    except Currency.DoesNotExist:
        return HttpResponse(return_response_text(failure_code, ErrorMessages.currency_not_supported))
    # except Exception, ex:
    #     Alert(type='002', reason=ex.message)
    #     return HttpResponse(return_response_text(failure_code, ErrorMessages.something_went_wrong))
    #     pass

    pass


@require_GET
def currency_limit_view(request, dimen='', currency=''):
    try:
        # is_ip_allowed(get_real_ip(request))

        if currency == '':
            raise OperationException(ErrorMessages.no_currency_selected)

        cur = Currency.objects.get(name=currency)

        if not cur:
            raise OperationException(ErrorMessages.currency_not_supported)

        default = lambda x: HttpResponse(return_response_text(failure_code, ErrorMessages.currency_not_supported))

        return {
                'maximum': lambda x: HttpResponse(return_response_text(success_code, x.maximum)),
                'minimum': lambda x: HttpResponse(return_response_text(success_code, x.minimum)),
                }.get(dimen, default)(cur)


    except OperationException, ex:
        return HttpResponse(return_response_text(failure_code, ex.message))
    except Currency.DoesNotExist:
        return HttpResponse(return_response_text(failure_code, ErrorMessages.currency_not_supported))
    except Exception, ex:
        Alert(type='002', reason=ex.message)
        return HttpResponse(return_response_text(failure_code, ErrorMessages.something_went_wrong))
        pass

    pass


@require_GET
def bank_holders_name_view(request, _code='', _account=''):
    try:
        # is_ip_allowed(get_real_ip(request))

        form = request.POST
        default_value = ''



        bank_code = cgi.escape(_code)
        account_number = cgi.escape(_account)

        if not bank_code:
            raise OperationException(ErrorMessages.no_bank_code_passed)

        if bank_code not in bank_codes:
            raise OperationException(ErrorMessages.invalid_bank_code)

        if account_number == '':
            raise OperationException(ErrorMessages.no_account_number_passed)


        bank = BankController()

        status, response = bank.get_account_holders_name(bank_code, account_number)

        if not status:
            raise OperationException(response)

        return HttpResponse(return_response_text(success_code, response))

    except OperationException, ex:
        if ex.message in ResultCodes.alertable_bank_codes:
            Alert.add_alert(type='001', reason=str(ex.message))
            ex.message = ErrorMessages.something_went_wrong
        return HttpResponse(return_response_text(failure_code, ex.message))
        pass
    except IpNotAllowedException, ex:
        return HttpResponse(return_response_text(failure_code, ErrorMessages.ip_not_allowed))
    except TypeError:
        return HttpResponse(return_response_text(failure_code, ErrorMessages.uncoded_values))
    except Exception, ex:
        Alert(type='002', reason=str(ex.message)+'in bank holders name view')
        return HttpResponse(return_response_text(failure_code, ErrorMessages.something_went_wrong))
        pass

    pass


@require_POST
def transfer_funds_view(request):
    try:
        # is_ip_allowed(get_real_ip(request))

        form = request.POST
        default_value = ''

        bank_code = Validation.is_not_empty(base64.b64decode(cgi.escape(form.get('bankcode', default_value))))
        account_name = Validation.is_not_empty(base64.b64decode(cgi.escape(form.get('accountname', default_value))))
        account_number = Validation.is_not_empty(base64.b64decode(cgi.escape(form.get('accountnumber', default_value))))
        narration = Validation.is_not_empty(base64.b64decode(cgi.escape(form.get('narration', default_value))))

        mtcn = Validation.is_not_empty(base64.b64decode(cgi.escape(form.get('mtcn'))))
        vendor_id = Validation.is_not_empty(base64.b64decode(cgi.escape(form.get('vendorid'))))
        signature = Validation.is_not_empty(cgi.escape(form.get('signature')))

        senders_fullname = Validation.is_not_empty(base64.b64decode(cgi.escape(form.get('sendersfullname', default_value))))
        senders_id_type = Validation.is_not_empty(base64.b64decode(cgi.escape(form.get('sendersidtype', default_value))))
        senders_id_number = Validation.is_not_empty(base64.b64decode(cgi.escape(form.get('sendersidnumber', default_value))))
        senders_country = Validation.is_not_empty(base64.b64decode(cgi.escape(form.get('senderscountry', default_value))))
        senders_state = Validation.is_not_empty(base64.b64decode(cgi.escape(form.get('sendersstate', default_value))))
        senders_mobile = Validation.is_not_empty(base64.b64decode(cgi.escape(form.get('sendersmobile', default_value))))

        amount = Validation.is_not_empty(base64.b64decode(cgi.escape(form.get('amount', default_value))))
        currency = Validation.is_not_empty(base64.b64decode(cgi.escape(form.get('currency'))))

        try:
            vendor = Mto.objects.get(vendor_id=vendor_id)
        except ObjectDoesNotExist:
            raise OperationException(ErrorMessages.invalid_vendor_id)

        try:
            cur = Currency.objects.get(name=currency)
        except ObjectDoesNotExist:
            raise OperationException(ErrorMessages.currency_not_supported)

        amount_in_float = float(amount)

        Currency.not_below_minimum_and_not_above_maximum(currency, amount_in_float)

        if bank_code not in bank_codes:
            raise OperationException(ErrorMessages.invalid_bank_code)

        if Transfer.mtcn_already_used(mtcn):
            raise OperationException(ErrorMessages.mtcn_already_used)

        if not Mto.is_sig_correct(vendor_id, mtcn, signature):
            raise OperationException(ErrorMessages.invalid_signature)



        exchange_rate = cur.exchange_rate
        amount_in_naira = amount_in_float * exchange_rate
        amount_in_naira_after_fees = calculate_deposit_minus_fees(amount_in_naira)


        import_bank = __import__('bankcontroller')
        bank = import_bank.BankController()

        status, response = bank.transfer_fund(mtcn, bank_code, account_number, account_name, amount, narration)

        transfer_status = '0'

        if status:
            transfer_status = '1'

        Transfer.add_transfer_to_db(
            mto=vendor,
            mtcn=mtcn,
            beneficiary_bank=bank_code,
            beneficiary_bank_account_number=account_number,
            beneficiary_bank_account_name=account_name,
            narration=narration,
            senders_fullname=senders_fullname,
            senders_id_type=senders_id_type,
            senders_id_number=senders_id_number,
            senders_country=senders_country,
            senders_state=senders_state,
            senders_mobile=senders_mobile,
            amount=amount_in_float,
            currency=currency,
            exchange_rate=exchange_rate,
            amount_in_naira=amount_in_naira,
            amount_in_naira_after_fees=amount_in_naira_after_fees,
            fees=get_set_fees(),
            status=transfer_status
        )

        if not status:
            raise OperationException(response)

        return HttpResponse(return_response_text(success_code, response))
    except FieldException, ex:
        return HttpResponse(return_response_text(failure_code, ex.message))
        pass
    except IpNotAllowedException, ex:
        return HttpResponse(return_response_text(failure_code, ErrorMessages.ip_not_allowed))
    except OperationException, ex:
        if ex.message in ResultCodes.alertable_bank_codes:
            Alert.add_alert(type='001', reason=str(ex.message))
            ex.message = ErrorMessages.something_went_wrong
        return HttpResponse(return_response_text(failure_code, ex.message))
        pass
    except ValueError, ex:
        return HttpResponse(return_response_text(failure_code, ErrorMessages.invalid_amount))
        pass
    except AmountAboveMaximumException, ex:
        return HttpResponse(return_response_text(failure_code, ex.message))
    except AmountBelowMinimumException, ex:
        return HttpResponse(return_response_text(failure_code, ex.message))
    except Exception, ex:
        Alert.add_alert(type='002', reason=str(ex.message)+'in transfer funds view')
        return HttpResponse(return_response_text(failure_code, ErrorMessages.something_went_wrong))
        pass





