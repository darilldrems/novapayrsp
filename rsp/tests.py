#testing git
import base64
from decimal import *

from django.test import TestCase
from django.test.client import Client
from django.core.exceptions import ObjectDoesNotExist


from errorcodes import ErrorMessages
from helpers import return_response_text, calculate_deposit_minus_fees, build_signatures
from bankcontroller import BankController
from bankresultcodes import ResultCodes
from models import Currency, Mto, Alert, Transfer


from models import Currency

from errors import AmountBelowMinimumException, AmountAboveMaximumException
from errorcodes import ErrorMessages

# Create your tests here.

success_code = '1'
failure_code = '0'

class ViewsTestCase(TestCase):
    def setUp(self):
        Currency.objects.create(name='dollar', exchange_rate=170.0, maximum=1000.0, minimum=100.0)
        Currency.objects.create(name='pound', exchange_rate=170.0, maximum=1000.0, minimum=100.0)

        Mto.objects.create(vendor_id='1234BN', name='eli', dev='2', ip_address='127.0.0.1', token='53thrt3535')
        pass

    def test_exchange_rate_view_with_fake_currency(self):
        c = Client()
        response = c.get('/rsp/api/exchange/hello/')
        self.assertEqual(response.content, return_response_text(failure_code, ErrorMessages.currency_not_supported))

    def test_exchange_rate_with_real_currency(self):
        c = Client()

        currency = 'dollar'

        response = c.get('/rsp/api/exchange/'+currency+'/')

        cur = Currency.objects.get(name=currency)

        self.assertEqual(response.content, return_response_text(success_code, cur.exchange_rate))

    def test_calculate_deposit_with_fake_currency(self):
        c = Client()

        currency = 'fake'
        amount = '100'

        response = c.get('/rsp/api/deposit/'+currency+'/'+amount+'/')

        self.assertEqual(response.content, return_response_text(failure_code, ErrorMessages.currency_not_supported))

    def test_calculate_deposit_with_real_amount(self):
        c = Client()

        currency = 'dollar'
        amount = '100'

        response = c.get('/rsp/api/deposit/'+currency+'/'+amount+'/')

        cur = Currency.objects.get(name=currency)

        self.assertEqual(response.content, return_response_text(success_code, calculate_deposit_minus_fees(cur.exchange_rate*100)))

    def test_calculate_deposit_with_amount_below_minimum(self):

        currency = 'dollar'

        cur = Currency.objects.get(name=currency)

        amount_below_minimum = int(cur.minimum - 10)

        c = Client()
        response = c.get('/rsp/api/deposit/'+currency+'/'+str(amount_below_minimum)+'/')

        self.assertEqual(response.content, return_response_text(failure_code, ErrorMessages.amount_below_minimum))

    def test_calculate_deposit_with_amount_above_maximum(self):

        currency = 'pound'

        cur = Currency.objects.get(name=currency)

        amount_above_maximum = int(cur.maximum + 10)

        c = Client()
        response = c.get('/rsp/api/deposit/'+currency+'/'+str(amount_above_maximum)+'/')

        self.assertEqual(response.content, return_response_text(failure_code, ErrorMessages.amount_above_maximum))

    def test_currency_limit_with_fake_currency(self):

        currency = 'cedis'
        limit = 'maximum'

        c = Client()
        response = c.get('/rsp/api/limit/'+currency+'/'+limit+'/')

        self.assertEqual(response.content, return_response_text(failure_code, ErrorMessages.currency_not_supported))

    def test_currency_limit_maximum_is_correct(self):

        currency = 'dollar'
        limit = 'maximum'

        cur = Currency.objects.get(name=currency)

        c = Client()
        response = c.get('/rsp/api/limit/'+currency+'/'+limit+'/')

        self.assertEqual(response.content, return_response_text(success_code, cur.maximum))

    def test_currency_limit_minimum_is_correct(self):
        currency = 'dollar'
        limit = 'minimum'

        cur = Currency.objects.get(name=currency)

        c = Client()
        response = c.get('/rsp/api/limit/'+currency+'/'+limit+'/')

        self.assertEqual(response.content, return_response_text(success_code, cur.minimum))

    def test_currency_limit_view_with_fake_limit(self):
        currency = 'dollar'
        limit = 'miaimum'

        c = Client()
        response = c.get('/rsp/api/limit/'+currency+'/'+limit+'/')

        self.assertEqual(response.content, return_response_text(failure_code, ErrorMessages.currency_not_supported))


    def test_bank_holders_name_view_with_invalid_bank_code(self):
        bank_code = '1110'
        account_number = '0022029103'

        client = Client()
        response = client.get('/rsp/api/holders/name/'+bank_code+'/'+account_number+'/')

        self.assertEqual(response.content, return_response_text(failure_code, ErrorMessages.invalid_bank_code))



    def test_bank_name_view_with_valid_data(self):
        bank_code = '214'
        account_number = '0569740035'

        client = Client()
        response = client.get('/rsp/api/holders/name/'+bank_code+'/'+account_number+'/')

        self.assertEqual(response.content, return_response_text(success_code, 'OMOTOSHO DAMOLA OLAWOLE'))


    def test_transfer_funds_view_with_invalid_vendor_id(self):
        c = Client()

        vendor_id = base64.b64encode('1234')
        mtcn = base64.b64encode('124098')
        signature = base64.b64encode(build_signatures('1234', '53thrt3535', '124098'))

        bank_code = base64.b64encode('214')
        account_number = base64.b64encode('0569740035')
        account_name = base64.b64encode('OMOTOSHO DAMOLA OLAWOLE')
        narration = base64.b64encode('testing transfer')

        senders_fullname = base64.b64encode('Ridwan olalere')
        senders_id_number = base64.b64encode('6345hhr')
        senders_id_type = base64.b64encode('IP')
        senders_country = base64.b64encode('uk')
        senders_state = base64.b64encode('london')
        senders_mobile = base64.b64encode('080763535')

        amount = base64.b64encode('200')
        currency = base64.b64encode('dollar')

        result = c.post('/rsp/api/transfer/', {
            'vendorid': vendor_id,
            'mtcn': mtcn,
            'signature': signature,
            'bankcode': bank_code,
            'accountnumber': account_number,
            'accountname': account_name,
            'narration': narration,
            'sendersfullname': senders_fullname,
            'sendersidnumber': senders_id_number,
            'sendersidtype': senders_id_type,
            'senderscountry': senders_country,
            'sendersstate': senders_state,
            'sendersmobile': senders_mobile,
            'amount': amount,
            'currency': currency,

        })

        self.assertEqual(result.content, return_response_text(failure_code, ErrorMessages.invalid_vendor_id))


    def test_transfer_funds_view_with_invalid_signature(self):
        c = Client()



        vendor_id = base64.b64encode('1234BN')
        mtcn = base64.b64encode('124098')
        signature = base64.b64encode(build_signatures('1234BN', '124098', '53thrt3535'))

        bank_code = base64.b64encode('214')
        account_number = base64.b64encode('0569740035')
        account_name = base64.b64encode('OMOTOSHO DAMOLA OLAWOLE')
        narration = base64.b64encode('testing transfer')

        senders_fullname = base64.b64encode('Ridwan olalere')
        senders_id_number = base64.b64encode('6345hhr')
        senders_id_type = base64.b64encode('IP')
        senders_country = base64.b64encode('uk')
        senders_state = base64.b64encode('london')
        senders_mobile = base64.b64encode('080763535')

        amount = base64.b64encode('200')
        currency = base64.b64encode('dollar')

        result = c.post('/rsp/api/transfer/', {
            'vendorid': vendor_id,
            'mtcn': mtcn,
            'signature': signature,
            'bankcode': bank_code,
            'accountnumber': account_number,
            'accountname': account_name,
            'narration': narration,
            'sendersfullname': senders_fullname,
            'sendersidnumber': senders_id_number,
            'sendersidtype': senders_id_type,
            'senderscountry': senders_country,
            'sendersstate': senders_state,
            'sendersmobile': senders_mobile,
            'amount': amount,
            'currency': currency,

        })

        self.assertEqual(result.content, return_response_text(failure_code, ErrorMessages.invalid_signature))


class ModelsTestCase(TestCase):
    def setUp(self):
        Mto.objects.create(vendor_id='020', name='testing', ip_address='127.0.0.1', token='0700090GH', dev='testing')
        Currency.objects.create(name='dollar', exchange_rate=170, minimum=50, maximum=2500)
        Transfer.objects.create(mto=Mto.objects.get(vendor_id='020'), mtcn='0202',
                                beneficiary_bank='044', beneficiary_bank_account_number='084746', beneficiary_bank_account_name='Olalere Ridwan',
                                narration='tuition fees', senders_fullname='Chinedu Okoli', senders_id_type='IP', senders_id_number='928263HGY67', senders_country='UK',
                                senders_state='London', senders_mobile='+23456672926', amount=100, currency='dollar', exchange_rate=170, amount_in_naira=17000, amount_in_naira_after_fees=16500,
                                fees=500, status=1)

    def test_is_sig_correct_with_invalid_vendor(self):
        result = Mto.is_sig_correct('070', '122290902', build_signatures('070', '0700090GH', '122290902'))
        self.assertFalse(result)

    def test_is_sig_correct_with_invalid_signature(self):
        vendor_id = '020'
        mtcn = '5543553434535'
        token = '0700090GH'

        result = Mto.is_sig_correct(vendor_id, mtcn, build_signatures(vendor_id, mtcn, token))

        self.assertFalse(result)
        pass

    def test_is_sig_correct_with_valid_values(self):
        vendor_id = '020'
        mtcn = '5543553434535'
        token = '0700090GH'

        result = Mto.is_sig_correct(vendor_id, mtcn, build_signatures(vendor_id, token, mtcn))

        self.assertTrue(result)

    def test_is_mto_ip_correct_with_invalid_vendor_id(self):
        vendor_id = '070'
        ip = '127.0.0.1'

        result = Mto.is_mto_ip_correct(vendor_id, ip)

        self.assertFalse(result)

    def test_is_mto_ip_correct_with_invalid_ip(self):
        vendor_id = '020'
        ip = '127.0.0.4'

        result = Mto.is_mto_ip_correct(vendor_id, ip)

        self.assertFalse(result)

    def test_is_mto_ip_correct_with_valid_data(self):
        vendor_id = '020'
        ip = '127.0.0.1'

        result = Mto.is_mto_ip_correct(vendor_id, ip)

        self.assertTrue(result)

    def test_amount_below_currency_minimum_works(self):

        self.assertRaises(AmountBelowMinimumException, lambda: Currency.not_below_minimum_and_not_above_maximum('dollar', 40.0))
        pass
    def test_amount_above_currency_maximum_works(self):
        self.assertRaises(AmountAboveMaximumException,
                          lambda: Currency.not_below_minimum_and_not_above_maximum('dollar', 2501.0))

    def test_mtcn_already_used_works(self):
        result = Transfer.mtcn_already_used('0202')
        self.assertTrue(result)

    def test_mtcn_already_used_with_inaccurate_mtcn(self):
        result = Transfer.mtcn_already_used('022')
        self.assertFalse(result)

    def test_add_transfer_to_db_works(self):
        mto_ = Mto.objects.create(vendor_id='070', name='testing', ip_address='127.0.0.1', token='0700090GH', dev='testing')



        result = Transfer.add_transfer_to_db(
            mto= mto_,
            mtcn='6736',
            beneficiary_bank='221',
            beneficiary_bank_account_number='897765345',
            beneficiary_bank_account_name='Chinedu Okoli',
            narration='salary',
            senders_fullname='Adekunle Osibodu',
            senders_id_type='IP',
            senders_id_number='74jUU6363',
            senders_country='United Kingdom',
            senders_state='London',
            senders_mobile='+23480877635',
            amount=200,
            currency='dollar',
            exchange_rate=170,
            amount_in_naira=34000,
            amount_in_naira_after_fees=33500,
            fees=500,
            status='1'
        )

        # self.assertEqual(1000, mto_.id)

        self.assertEqual(result, '6736')

    def test_add_alert_works(self):
        a = Alert.add_alert(type='001', reason='testing')
        self.assertNotEqual(a, 'null')
        pass


class HelpersTestCase(TestCase):
    pass

class BankControllersTestCase(TestCase):

    def test_get_account_balance_method(self):
        bank = BankController()

        amount = bank.get_account_balance()

        self.assertNotEqual(amount, ResultCodes.account_balance_error_code)

    def test_get_account_holders_name_with_accurate_data(self):
        bank_code = '050'
        account_number = '0022029103'

        bank = BankController()

        status, result = bank.get_account_holders_name(bank_code, account_number)

        self.assertEqual((status, result), (True, 'AMINU  ISHOLA YUSUF'))

    def test_get_account_holders_name_with_inaccurate_bank_code(self):
        bank_code = '000'
        account_number = '0022029103'

        bank = BankController()

        result = bank.get_account_holders_name(bank_code, account_number)

        self.assertEqual(result, (False, ResultCodes.unknown_bank_code))

    def test_transfer_fund_with_accurate_data(self):
        bank_code = '050'
        account_number = '0022029103'
        amount = '12500'
        mtcn = '010'
        narration = 'testing transfers'

        bank = BankController()

        previous_balance = bank.get_account_balance()

        status, r = bank.get_account_holders_name(bank_code, account_number)

        result = bank.transfer_fund(mtcn, bank_code, account_number, r, amount, narration)

        new_balance = bank.get_account_balance()

        self.assertEqual(result, (True, mtcn+':'+amount))
        self.assertNotEqual(previous_balance, new_balance)



