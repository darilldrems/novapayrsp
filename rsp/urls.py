from django.conf.urls import patterns, url


urlpatterns = patterns('rsp.views',
    url(r'^api/exchange/(?P<currency>\w+)/$', 'exchange_rate_view'),
    url(r'^api/deposit/(?P<currency>\w+)/(?P<amount>\d+)/$', 'calculate_deposit_view'),
    url(r'^api/limit/(?P<currency>\w+)/(?P<dimen>\w+)/$', 'currency_limit_view'),
    url(r'^api/holders/name/(?P<_code>\w+)/(?P<_account>\w+)/$', 'bank_holders_name_view'),
    url(r'^api/transfer/$', 'transfer_funds_view'),

)