class ResultCodes:
    def __init__(self):
        pass
    account_balance_error_code = '060'
    success_code = '00'
    server_down_error_code = '91'
    invalid_sender = '03'
    do_not_honor = '05'
    invalid_account = '07'
    account_name_mismatch = '08'
    request_processing_in_progress = '09'
    invalid_transaction = '12'
    invalid_amount = '13'
    invalid_batch_number = '14'
    invalid_session_or_record_number = '15'
    unknown_bank_code = '16'
    invalid_channel = '17'
    wrong_method_call = '18'
    no_action_taken = '21'
    unable_to_locate_record = '25'
    duplicate_record = '26'
    format_error = '30'
    below_minimum_balance = '32'
    suspected_fraud = '34'
    contact_sending_bank = '35'
    no_sufficient_funds = '51'
    transaction_not_permitted_to_sender = '57'
    transaction_not_permitted_on_channel = '58'
    transfer_limit_exceeded = '61'
    security_violation = '63'
    exceeds_withdrawal_frequency = '65'
    response_received_too_late = '68'
    beneficiary_bank_not_available = '91'
    routing_error = '92'
    duplicate_transaction = '94'
    system_malfunction = '96'

    alertable_bank_codes = ['92', '68', '09', '05', '03', '14', '18', '25', '30', '34', '35', '51', '58', '61', '63', '65', '68']





