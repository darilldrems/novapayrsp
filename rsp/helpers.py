from errors import IpNotAllowedException



def return_response_text(code, result):
    return code + ' | ' + str(result)


def is_ip_allowed(real_ip):
    #use ip aware middleware
    try:
        ip = real_ip

        import_model = __import__('models')

        if ip is not None:
            vendor_ip = import_model.Mto.objects.get(ip_address=ip)

            if vendor_ip.dev == 'testing':
                raise IpNotAllowedException('')

        else:
            raise IpNotAllowedException('')
    except import_model.Mto.DoesNotExist:
        raise IpNotAllowedException('')
        pass


    pass

fees = 250
def calculate_deposit_minus_fees(amount_in_naira):
    return amount_in_naira - fees

def get_set_fees():
    return fees



def build_signatures(vendor_id, token, mtcn):
    hash_sha224 = __import__('hashlib')
    signature = hash_sha224.sha224(vendor_id+token+mtcn).hexdigest()
    return signature