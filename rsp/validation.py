from errors import FieldException

class Validation:
    def __init__(self):
        pass

    @classmethod
    def is_not_empty(cls, val):
        if not val:
            raise FieldException('')

        return val


