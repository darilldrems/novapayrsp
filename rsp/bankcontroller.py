from suds.client import Client


import base64
import hashlib


from bankcodes import bank_codes
from bankresultcodes import ResultCodes

import socket


from agent_portal.helpers import send_sms

class BankController:
    if socket.gethostname() == 'b6gc-ywm6.accessdomain.com':
        url = 'https://apps.wemabank.com/NovatiaService/novatia.asmx?wsdl' #live environment
    else:
        url = 'https://webservice.wemabank.com/internetbank/novatia.asmx?wsdl' #test environment
    channel_code = '1'

    vendor_id = 'Calibre123'
    signature_salt = 'Calibre'

    maximum_transfers_per_day = 2000000
    alert_number_1 = '+2348064474405'
    alert_number_2 = '+2349098090424'



    calibre_host_account_number = '0122347520'
    novatia_host_account_number = '0122412118'

    calibre_originator_name = 'CALIBRE CONSULTING'
    novatia_originator_name = 'NOVATIA NP'



    def __init__(self, url=None):
        self.balance = 0.0
        if not url:
            self.client = Client(BankController.url)
        else:
            self.client = Client(url)

        pass

    def print_services(self):
        return self.client

    def get_account_balance(self):
        result = self.client.service.getAccountBalance()

        if result.strip() != ResultCodes.account_balance_error_code:
           self.balance = result
        return self.balance

    def get_account_holders_name(self, bank_code, account_number):

        if bank_code not in bank_codes:
            return False, ResultCodes.unknown_bank_code

        result = self.client.service.nameenquiry(bank_code, account_number, BankController.channel_code)

        code, response = result.split('|')

        if code.strip() == ResultCodes.success_code:
            return True, response

        return False, code

    def get_maximum_transfers_per_day(self):
        result = self.client.service.GetCummulativeAmount_novatia(BankController.novatia_host_account_number)

        return result.split('|')[1].strip()

    def get_total_transfers_today(self):

        result = self.client.service.GetCummulativeAmount_novatia(BankController.novatia_host_account_number)

        return result.split('|')[0].strip()

    def transfer_fund(self, mtcn, destination_bank_code, account_number, account_name, amount, narration, source):

        if destination_bank_code not in bank_codes:
            return False, 'incorrect bank code'

        myDestinationBankCode = destination_bank_code
        myChannelCode = BankController.channel_code
        myAccountName = account_name

        myNarration = narration
        myPaymentReference = mtcn
        myAmount = str(amount)
        vendorID = BankController.vendor_id
        myCustDestinationAccountNumber = account_number


        myOriginatorName = BankController.novatia_originator_name
        source_account = BankController.novatia_host_account_number

        if source == "C":
            myOriginatorName = BankController.calibre_originator_name
            source_account = BankController.calibre_host_account_number






        signature = hashlib.md5(vendorID+myAccountName+myCustDestinationAccountNumber+BankController.signature_salt).hexdigest()

        myDestinationBankCode = base64.b64encode(myDestinationBankCode)
        myChannelCode = base64.b64encode(myChannelCode)
        myPaymentReference = base64.b64encode(myPaymentReference)
        vendorID = base64.b64encode(vendorID)
        myAccountName = base64.b64encode(myAccountName)
        myCustDestinationAccountNumber = base64.b64encode(myCustDestinationAccountNumber)
        myAmount = base64.b64encode(myAmount)

        result = self.client.service.fundtransfers(
            myDestinationBankCode, myChannelCode, myCustDestinationAccountNumber, myAccountName, myOriginatorName,
            myNarration, myPaymentReference, myAmount, vendorID, signature, source_account
        )

        print result

        if 'fundtransfersResult = "00"' in str(result):
            send_sms(BankController.alert_number_1, "transfered: "+amount+"; ref number:"+mtcn)
            send_sms(BankController.alert_number_2, "transfered: "+amount+"; ref number:"+mtcn)
            return True, mtcn + ':' + base64.b64decode(myAmount)

        return False, result



        pass