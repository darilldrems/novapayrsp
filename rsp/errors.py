class FieldException(Exception):
    def __init__(self, mes):
        self.message = mes


class OperationException(Exception):
    def __init__(self, mess):
        self.message = mess

class IpNotAllowedException(Exception):
    def __init__(self, mess):
        self.message = mess

class AmountAboveMaximumException(Exception):
    def __init__(self, mess):
        self.message = mess

class AmountBelowMinimumException(Exception):
    def __init__(self, mess):
        self.message = mess

class InvalidAmountException(Exception):
    def __init__(self, mess):
        self.message = mess