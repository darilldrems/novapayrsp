# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Mto'
        db.create_table(u'rsp_mto', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('vendor_id', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('created_on', self.gf('django.db.models.fields.DateField')(auto_now_add=True, blank=True)),
            ('ip_address', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('token', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('dev', self.gf('django.db.models.fields.CharField')(max_length=10)),
        ))
        db.send_create_signal(u'rsp', ['Mto'])

        # Adding model 'Currency'
        db.create_table(u'rsp_currency', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('exchange_rate', self.gf('django.db.models.fields.DecimalField')(max_digits=65, decimal_places=2)),
            ('minimum', self.gf('django.db.models.fields.DecimalField')(default=0.0, max_digits=65, decimal_places=2)),
            ('maximum', self.gf('django.db.models.fields.DecimalField')(default=0.0, max_digits=65, decimal_places=2)),
        ))
        db.send_create_signal(u'rsp', ['Currency'])

        # Adding model 'Transfer'
        db.create_table(u'rsp_transfer', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('mto', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['rsp.Mto'])),
            ('mtcn', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('beneficiary_bank', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('beneficiary_bank_account_number', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('beneficiary_bank_account_name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('narration', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('senders_fullname', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('senders_id_type', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('senders_id_number', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('senders_country', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('senders_state', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('senders_mobile', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('amount', self.gf('django.db.models.fields.DecimalField')(max_digits=65, decimal_places=2)),
            ('currency', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('exchange_rate', self.gf('django.db.models.fields.DecimalField')(max_digits=65, decimal_places=2)),
            ('amount_in_naira', self.gf('django.db.models.fields.DecimalField')(max_digits=65, decimal_places=2)),
            ('amount_in_naira_after_fees', self.gf('django.db.models.fields.DecimalField')(max_digits=65, decimal_places=2)),
            ('fees', self.gf('django.db.models.fields.DecimalField')(default=0.0, max_digits=65, decimal_places=2)),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'rsp', ['Transfer'])

        # Adding model 'Alert'
        db.create_table(u'rsp_alert', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=5)),
            ('reason', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'rsp', ['Alert'])


    def backwards(self, orm):
        # Deleting model 'Mto'
        db.delete_table(u'rsp_mto')

        # Deleting model 'Currency'
        db.delete_table(u'rsp_currency')

        # Deleting model 'Transfer'
        db.delete_table(u'rsp_transfer')

        # Deleting model 'Alert'
        db.delete_table(u'rsp_alert')


    models = {
        u'rsp.alert': {
            'Meta': {'object_name': 'Alert'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'reason': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '5'})
        },
        u'rsp.currency': {
            'Meta': {'object_name': 'Currency'},
            'exchange_rate': ('django.db.models.fields.DecimalField', [], {'max_digits': '65', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'maximum': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '65', 'decimal_places': '2'}),
            'minimum': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '65', 'decimal_places': '2'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'rsp.mto': {
            'Meta': {'object_name': 'Mto'},
            'created_on': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'dev': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip_address': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'token': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'vendor_id': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'rsp.transfer': {
            'Meta': {'object_name': 'Transfer'},
            'amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '65', 'decimal_places': '2'}),
            'amount_in_naira': ('django.db.models.fields.DecimalField', [], {'max_digits': '65', 'decimal_places': '2'}),
            'amount_in_naira_after_fees': ('django.db.models.fields.DecimalField', [], {'max_digits': '65', 'decimal_places': '2'}),
            'beneficiary_bank': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'beneficiary_bank_account_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'beneficiary_bank_account_number': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'currency': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'exchange_rate': ('django.db.models.fields.DecimalField', [], {'max_digits': '65', 'decimal_places': '2'}),
            'fees': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '65', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mtcn': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'mto': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['rsp.Mto']"}),
            'narration': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'senders_country': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'senders_fullname': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'senders_id_number': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'senders_id_type': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'senders_mobile': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'senders_state': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['rsp']