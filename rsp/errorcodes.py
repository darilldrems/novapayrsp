
class ErrorMessages:
    def __init__(self):
        pass
    no_currency_selected = '501'
    currency_not_supported = '502'
    amount_below_minimum = '522'
    amount_above_maximum = '544'
    invalid_amount = '500'
    dimension_not_supported = '511'
    no_bank_code_passed = '599'
    no_account_number_passed = '598'
    invalid_bank_code = '16'
    mtcn_already_used = '555'
    invalid_signature = '585'
    something_went_wrong = '900'
    ip_not_allowed = '999'
    uncoded_values = '990'
    invalid_vendor_id = '980'