import urllib2
import logging
import os

def download_csv(from_, to):
    try:
        path_to_file = os.getcwd() + to
        downloaded_file = urllib2.urlopen(from_)
        output_file = open(path_to_file, 'wb')
        output_file.write(downloaded_file.read())
        output_file.close()
    except Exception:
        logging.exception('download csv exception')

