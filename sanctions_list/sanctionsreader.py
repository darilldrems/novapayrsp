import os
import csv
import logging

class SanctionsReader:
    source = os.getcwd() + '/list/sanctions_list.csv'
    params_and_index = {
        'Name 6': 0,
        'Name 1': 1,
        'Name 2': 2,
        'Name 3': 3,
        'Name 4': 4,
        'Name 5': 5,
        'Title': 6,
        'Date of Birth': 7,
        'Town of Birth': 8,
        'Country of Birth': 9,
        'Nationality': 10,
        'Passport Details': 11,
        'NI Number': 12,
        'Position': 13,
        'Address 1': 14,
        'Address 2': 15,
        'Address 3': 16,
        'Address 4': 17,
        'Address 5': 18,
        'Address 6': 19,
        'Post/Zip Code': 20,
        'Country': 21,
        'Other information': 22,
        'Group Type': 23,
        'Alias Type': 24,
        'Regime': 25,
        'Listed on': 26,
        'Last Updated': 27,
        'Group ID': 28,

    }

    @staticmethod
    def person_in_list(data):
        try:
            with open(SanctionsReader.source) as csv_file:
                file_reader = csv.reader(csv_file)
                is_in_list = False
                for row in file_reader:
                    in_row = []
                    for type_, value in data.items():
                        if row[SanctionsReader.params_and_index.get(type_)].lower() == value.lower():
                            in_row.append(True)
                        else:
                            in_row.append(False)
                    if all(in_row):
                        is_in_list = True

                return is_in_list

        except Exception:
            logging.exception('Sanctions Reader Exception')
            pass