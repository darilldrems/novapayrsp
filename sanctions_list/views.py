from django.http import HttpResponse

from downloader import download_csv

def download_sanctions_list(request):
    sanctions_list_url = 'http://hmt-sanctions.s3.amazonaws.com/sanctionsconlist.csv'
    local_sanctions_list_file_name = '/sanctions_list/list/sanctions_list.csv'
    download_csv(sanctions_list_url, local_sanctions_list_file_name)
    return HttpResponse()

