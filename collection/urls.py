from django.conf.urls import patterns, url


urlpatterns = patterns('collection.views',

    url(r'^login/$', 'login_view', name='collection_login'),
    url(r'^dashboard/$', 'dashboard_view', name="collection_dashboard"),
    url(r'^register/$', 'register_view', name='collection_register'),
    url(r'^logout/$', 'logout_view', name='collection_logout'),
)