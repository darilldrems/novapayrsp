from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.contrib.auth import authenticate, logout, login
from django.contrib.auth.decorators import user_passes_test
from django.core.paginator import PageNotAnInteger, EmptyPage, Paginator


from agent_portal.forms import LoginForm
from helpers import permitted_aggregator

from models import Payment, Settlement

# Create your views here.

def login_view(request, template_name="collection/login.html"):

    form = LoginForm()

    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')

            user = authenticate(username=username, password=password)

            if user and user.is_active:
                if user.aggregator:
                    login(request, user)
                    # return HttpResponse("loggedin")
                    return HttpResponseRedirect(reverse('collection_dashboard'))

    context = {
        'form': form,
    }

    # return HttpResponse("didnt recognize go")
    return render(request, template_name, context)


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('collection_login'))


@user_passes_test(permitted_aggregator, login_url='/collection/login/')
def dashboard_view(request, template_name="collection/dashboard.html", page="dashboard"):

    payments_list = Payment.objects.filter(aggregator=request.user.aggregator)
    settlements_list = Settlement.objects.filter(aggregator=request.user.aggregator)

    paginator = Paginator(payments_list, 15)
    paginator2 = Paginator(settlements_list, 15)

    page_no = request.GET.get('p')

    try:
        payments = paginator.page(page_no)
        settlements = paginator2.page(page_no)
    except PageNotAnInteger:
        payments = paginator.page(1)
        settlements = paginator2.page(1)
    except EmptyPage:
        payments = paginator.page(paginator.num_pages)
        settlements = paginator2.page(paginator2.num_pages)



    context = {
        'page': page,
        'wallets': request.user.aggregator.wallet.all(),
        'payments': payments,
        'settlements': settlements,
        'agent': request.user.agent,

    }

    return render(request, template_name, context)


def register_view(request, template_name="collection/register.html"):

    context = {

    }

    return render(request, template_name, context)


