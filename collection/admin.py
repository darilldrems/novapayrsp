from django.contrib import admin

from models import Aggregator, Wallet, Payment, Currency, Settlement

# Register your models here.

admin.site.register(Aggregator)
admin.site.register(Wallet)
admin.site.register(Payment)
admin.site.register(Currency)
admin.site.register(Settlement)
