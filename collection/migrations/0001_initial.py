# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Currency'
        db.create_table(u'collection_currency', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('exchange_rate', self.gf('django.db.models.fields.DecimalField')(max_digits=65, decimal_places=2)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('symbol', self.gf('django.db.models.fields.CharField')(max_length=30)),
        ))
        db.send_create_signal(u'collection', ['Currency'])

        # Adding model 'Wallet'
        db.create_table(u'collection_wallet', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('currency', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['collection.Currency'])),
            ('balance', self.gf('django.db.models.fields.DecimalField')(default='0', max_digits=65, decimal_places=2)),
        ))
        db.send_create_signal(u'collection', ['Wallet'])

        # Adding model 'Aggregator'
        db.create_table(u'collection_aggregator', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True)),
            ('location', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('tel1', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('tel2', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('identifier', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal(u'collection', ['Aggregator'])

        # Adding M2M table for field wallet on 'Aggregator'
        m2m_table_name = db.shorten_name(u'collection_aggregator_wallet')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('aggregator', models.ForeignKey(orm[u'collection.aggregator'], null=False)),
            ('wallet', models.ForeignKey(orm[u'collection.wallet'], null=False))
        ))
        db.create_unique(m2m_table_name, ['aggregator_id', 'wallet_id'])

        # Adding model 'Payment'
        db.create_table(u'collection_payment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('payee', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('branch', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('amount_in_naira', self.gf('django.db.models.fields.DecimalField')(max_digits=65, decimal_places=2)),
            ('amount_in_currency', self.gf('django.db.models.fields.DecimalField')(max_digits=65, decimal_places=2)),
            ('currency', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['collection.Currency'])),
            ('exchange_rate', self.gf('django.db.models.fields.DecimalField')(max_digits=65, decimal_places=2)),
            ('aggregator', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['collection.Aggregator'])),
            ('bank_time', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('time_recorded', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'collection', ['Payment'])

        # Adding model 'Settlement'
        db.create_table(u'collection_settlement', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('amount', self.gf('django.db.models.fields.DecimalField')(max_digits=65, decimal_places=2)),
            ('currency', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['collection.Currency'])),
            ('date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('aggregator', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['collection.Aggregator'])),
        ))
        db.send_create_signal(u'collection', ['Settlement'])


    def backwards(self, orm):
        # Deleting model 'Currency'
        db.delete_table(u'collection_currency')

        # Deleting model 'Wallet'
        db.delete_table(u'collection_wallet')

        # Deleting model 'Aggregator'
        db.delete_table(u'collection_aggregator')

        # Removing M2M table for field wallet on 'Aggregator'
        db.delete_table(db.shorten_name(u'collection_aggregator_wallet'))

        # Deleting model 'Payment'
        db.delete_table(u'collection_payment')

        # Deleting model 'Settlement'
        db.delete_table(u'collection_settlement')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'collection.aggregator': {
            'Meta': {'object_name': 'Aggregator'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identifier': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'tel1': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'tel2': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'}),
            'wallet': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['collection.Wallet']", 'symmetrical': 'False'})
        },
        u'collection.currency': {
            'Meta': {'object_name': 'Currency'},
            'exchange_rate': ('django.db.models.fields.DecimalField', [], {'max_digits': '65', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'symbol': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'collection.payment': {
            'Meta': {'ordering': "['-time_recorded']", 'object_name': 'Payment'},
            'aggregator': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collection.Aggregator']"}),
            'amount_in_currency': ('django.db.models.fields.DecimalField', [], {'max_digits': '65', 'decimal_places': '2'}),
            'amount_in_naira': ('django.db.models.fields.DecimalField', [], {'max_digits': '65', 'decimal_places': '2'}),
            'bank_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'branch': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collection.Currency']"}),
            'exchange_rate': ('django.db.models.fields.DecimalField', [], {'max_digits': '65', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'payee': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'time_recorded': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'collection.settlement': {
            'Meta': {'object_name': 'Settlement'},
            'aggregator': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collection.Aggregator']"}),
            'amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '65', 'decimal_places': '2'}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collection.Currency']"}),
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'collection.wallet': {
            'Meta': {'object_name': 'Wallet'},
            'balance': ('django.db.models.fields.DecimalField', [], {'default': "'0'", 'max_digits': '65', 'decimal_places': '2'}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collection.Currency']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['collection']