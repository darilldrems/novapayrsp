from django.core.exceptions import ObjectDoesNotExist

def permitted_aggregator(user):
    try:
        if user.is_active and user.is_authenticated():
            if user.aggregator:
                return True

        return False
    except ObjectDoesNotExist:
        return False