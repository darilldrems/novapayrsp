from django.db import models

from django.contrib.auth.models import User

from decimal import Decimal

# Create your models here.

class Currency(models.Model):
    exchange_rate = models.DecimalField(max_digits=65, decimal_places=2)
    name = models.CharField(max_length=20)
    symbol = models.CharField(max_length=30)

    def __unicode__(self):
        return self.name

class Wallet(models.Model):
    currency = models.ForeignKey(Currency)
    balance = models.DecimalField(decimal_places=2, max_digits=65, editable=False, default=Decimal('0'))

    def __unicode__(self):
        return self.currency.name


class Aggregator(models.Model):
    user = models.OneToOneField(User)
    location = models.CharField(max_length=100, blank=True, null=True)
    tel1 = models.CharField(max_length=100)
    tel2 = models.CharField(max_length=100, blank=True, null=True)
    identifier = models.CharField(max_length=200)
    wallet = models.ManyToManyField(Wallet)


    def __unicode__(self):
        return self.user.username




class Payment(models.Model):
    payee = models.CharField(max_length=100)
    branch = models.CharField(max_length=100)
    amount_in_naira = models.DecimalField(decimal_places=2, max_digits=65)
    amount_in_currency = models.DecimalField(decimal_places=2, max_digits=65)
    currency = models.ForeignKey(Currency)
    exchange_rate = models.DecimalField(max_digits=65, decimal_places=2)
    aggregator = models.ForeignKey(Aggregator)
    bank_time = models.DateTimeField(null=True, blank=True)
    time_recorded = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-time_recorded']

    def __unicode__(self):
        return self.pk

class Settlement(models.Model):
    amount = models.DecimalField(decimal_places=2, max_digits=65)
    currency = models.ForeignKey(Currency)
    date = models.DateTimeField(auto_now_add=True)
    aggregator = models.ForeignKey(Aggregator)
    pass
