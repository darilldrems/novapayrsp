from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'novapay.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^collection/', include('collection.urls')),
    url(r'^rsp/', include('rsp.urls')),
    url(r'^admin/', include(admin.site.urls)),
    # url(r'^agent/', include('agent_portal.urls')),
    url(r'^pages/', include('django.contrib.flatpages.urls')),
    url(r'settlement/', include('settlement.urls')),
    url(r'session_security/', include('session_security.urls')),
    url(r'sanctions_app/', include('sanctions_list.urls')),
    url(r'^', include('agent_portal.urls')),



)
