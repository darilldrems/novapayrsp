from django.conf.urls import patterns, url

urlpatterns = patterns('settlement.views',
    url(r'dashboard/$', 'dashboard_view', name='settlement_dashboard'),
    url(r'transfers/$', 'transfers_view', name='settlement_transfers'),
    url(r'transfer/$', 'transfer_view', name='settlement_transfer'),
    url(r'agents/$', 'agents_view', name='settlement_agents'),
    url(r'agent/$', 'agent_view', name='settlement_agent'),
    url(r'funds/$', 'funds_view', name='settlement_funds'),
    url(r'fund_action/$', 'accept_reject_funds', name='settlement_fund_action'),
    url(r'payments/$', 'payments_view', name='settlement_payments'),
    url(r'add_payment/$', 'add_payment', name='settlement_add_payment'),
    url(r'mtos/$', 'mtos_view', name='settlement_mtos'),
    url(r'mto/$', 'mto_view', name='settlement_mto'),
    url(r'countries/$', 'country_view', name='settlement_country'),
    url(r'rates/$', 'rates_view', name='settlement_rates'),
    url(r'currencies/$', 'currencies_view', name="settlement_currencies"),
    url(r'rate/edit/$', 'edit_rate', name="settlement_edit_rate"),
    url(r'currency/edit/$', 'edit_currency', name="settlement_edit_currency"),
    url(r'^', 'login_view', name='settlement_login'),

)