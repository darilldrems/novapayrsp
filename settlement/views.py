from django.shortcuts import render_to_response, redirect, render
from django.contrib.auth.decorators import user_passes_test
from django.core.paginator import PageNotAnInteger, Paginator, EmptyPage
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.db.models import Sum
from django.db import transaction
from django.http import HttpResponseRedirect
from django.contrib.auth import login, authenticate

from rsp.bankcontroller import BankController
from agent_portal.models import Transfer, Fund, Agent, Currency, Mto, Payment, Schedule, Country, Rate

from datetime import date
from decimal import Decimal

from helpers import is_staff
from agent_portal.forms import MtoForm, ScheduleForm, SettlementLoginForm, CountryForm, RateForm, CurrencyForm

# Create your views here.

@user_passes_test(is_staff, login_url='/admin')
def dashboard_view(request, template_name='settlement/dashboard.html', title='Dashboards'):

    bank = BankController()

    account_balance = bank.get_account_balance()
    recent_transfers = Transfer.objects.all()[:10]
    total_agents = Agent.objects.all().count()
    fx = Currency.objects.all()
    recent_funds = Fund.objects.all()[:10]
    total_transfers_today = Transfer.total_naira_transfered_today()

    context = {
        'title': title,
        'account_balance': account_balance,
        'recent_transfers': recent_transfers,
        'user': request.user,
        'fx': fx,
        'recent_funds': recent_funds,
        'total_agents': total_agents,
        'total_transfers_today': total_transfers_today,
    }
    return render_to_response(template_name, context)

@user_passes_test(is_staff, login_url='/admin')
def transfers_view(request, template_name='settlement/transfers.html', title='Transfers'):
    transfer_list = Transfer.objects.all()

    if request.GET.get('from') or request.GET.get('to'):
        from_ = request.GET.get('from').split('-')
        to_ = request.GET.get('to').split('-')
        if not request.GET.get('to'):
            from_date = date(int(from_[0]), int(from_[1]), int(from_[2]))
            transfer_list = Transfer.objects.filter(time__gt=from_date)
        else:
            from_date = date(int(from_[0]), int(from_[1]), int(from_[2]))
            to_date = date(int(to_[0]), int(to_[1]), int(to_[2]))
            transfer_list = Transfer.objects.filter(time__range=(from_date, to_date))

    total_transfers_in_local_currency = transfer_list.aggregate(Sum('amount_in_local_currency'))
    total_wema_fees_paid = transfer_list.aggregate(Sum('wema_fee'))
    total_fees_collected_by_platform = transfer_list.aggregate(Sum('platform_fees'))
    total_fess_collected_by_agent = transfer_list.aggregate(Sum('agent_fees'))
    total_pending_settlement = transfer_list.filter(settlement_status='pending').aggregate(Sum('amount_in_local_currency'))
    total_number_of_transactions = transfer_list.count()

    paginator = Paginator(transfer_list, 10)
    page = request.GET.get('page')

    try:
        transfers = paginator.page(page)
    except PageNotAnInteger:
        transfers = paginator.page(1)
    except EmptyPage:
        transfers = paginator.page(paginator.num_pages)

    context = {
        'user': request.user,
        'title': title,
        'transfers': transfers,
        'date_from':request.GET.get('from'),
        'date_to':request.GET.get('to'),
        'transfers_sum_in_naira': total_transfers_in_local_currency['amount_in_local_currency__sum'],
        'total_platform_fees': total_fees_collected_by_platform['platform_fees__sum'],
        'total_wema_fees': total_wema_fees_paid['wema_fee__sum'],
        'total_fees_collected_by_agent': total_fess_collected_by_agent['agent_fees__sum'],
        'total_pending_settlement': total_pending_settlement['amount_in_local_currency__sum'],
        'total_transactions': total_number_of_transactions,
    }
    return render_to_response(template_name, context)

@user_passes_test(is_staff, login_url='/admin')
def transfer_view(request, template_name='settlement/transfer.html', title='Transfer'):
    key = request.GET.get('key')


    try:
        transfer = Transfer.objects.get(pk=key)
    except ObjectDoesNotExist:
        redirect(reverse('settlement_dashboard'))

    context = {
        'user': request.user,
        'title': title,
        'transfer': transfer,
    }
    return render_to_response(template_name, context)
    pass

@user_passes_test(is_staff, login_url='/admin')
def agents_view(request, template_name='settlement/agents.html', title='Agents'):

    agents_list = Agent.objects.all()

    paginator = Paginator(agents_list, 10)
    page = request.GET.get('page')

    try:
        agents = paginator.page(page)
    except PageNotAnInteger:
        agents = paginator.page(1)
    except EmptyPage:
        agents = paginator.page(paginator.num_pages)

    mtos = Mto.objects.all()
    context = {
        'user': request.user,
        'mtos': mtos,
        'agents': agents,
        'title': title,
    }
    return render_to_response(template_name, context)

@user_passes_test(is_staff, login_url='/admin')
def agent_view(request, template_name='settlement/agent_detail.html', title='Agents'):
    pk = request.GET.get('key')

    try:
        agent = Agent.objects.get(pk=pk)
    except ObjectDoesNotExist:
        return redirect(reverse('settlement_dashboard'))

    agents_transfer_list = Transfer.objects.filter(agent__pk=pk)

    if request.GET.get('from') or request.GET.get('to'):
        from_ = request.GET.get('from').split('-')
        to_ = request.GET.get('to').split('-')
        if not request.GET.get('to'):
            from_date = date(int(from_[0]), int(from_[1]), int(from_[2]))
            agents_transfer_list = agents_transfer_list.filter(time__gt=from_date)
        else:
            from_date = date(int(from_[0]), int(from_[1]), int(from_[2]))
            to_date = date(int(to_[0]), int(to_[1]), int(to_[2]))
            agents_transfer_list = agents_transfer_list.filter(time__range=(from_date, to_date))

    total_amount_transferred_in_naira = agents_transfer_list.aggregate(Sum('amount_in_local_currency'))
    total_number_of_transactions = agents_transfer_list.count()
    total_amount_transferred = agents_transfer_list.aggregate(Sum('amount_in_currency'))
    total_platform_fees = agents_transfer_list.aggregate(Sum('platform_fees'))
    total_agent_fees = agents_transfer_list.aggregate(Sum('agent_fees'))
    total_fees = agents_transfer_list.aggregate(Sum('fees_collected'))
    amount_expected = agents_transfer_list.aggregate(Sum('amount_expected'))

    p = Paginator(agents_transfer_list, 10)
    page = request.GET.get('page')

    try:
        transfers = p.page(page)
    except PageNotAnInteger:
        transfers = p.page(1)
    except EmptyPage:
        transfers = p.page(p.num_pages)

    context = {
        'user': request.user,
        'transfers': transfers,
        'agent': agent,
        'date_from':request.GET.get('from'),
        'date_to':request.GET.get('to'),
        'total_amount_transferred_in_naira': total_amount_transferred_in_naira['amount_in_local_currency__sum'],
        'total_number_of_transactions': total_number_of_transactions,
        'total_amount_transferred': total_amount_transferred['amount_in_currency__sum'],
        'total_platform_fees': total_platform_fees['platform_fees__sum'],
        'total_agent_fees': total_agent_fees['agent_fees__sum'],
        'total_fees': total_fees['fees_collected__sum'],
        'total_amount_expected': amount_expected['amount_expected__sum'],
    }
    return render_to_response(template_name, context)

@user_passes_test(is_staff, login_url='/admin')
def funds_view(request, template_name='settlement/funds.html', title=''):

    funds_list = Fund.objects.all()

    if request.GET.get('status') is 'y':
        funds_list = funds_list.filter(status='p')

    if request.GET.get('from') or request.GET.get('to'):
        from_ = request.GET.get('from').split('-')
        to_ = request.GET.get('to').split('-')
        if not request.GET.get('to'):
            from_date = date(int(from_[0]), int(from_[1]), int(from_[2]))
            funds_list = funds_list.filter(requested_on__gt=from_date)
        else:
            from_date = date(int(from_[0]), int(from_[1]), int(from_[2]))
            to_date = date(int(to_[0]), int(to_[1]), int(to_[2]))
            funds_list = funds_list.filter(requested_on__range=(from_date, to_date))

    paginator = Paginator(funds_list, 10)
    page = request.GET.get('page')

    try:
        funds = paginator.page(page)
    except PageNotAnInteger:
        funds = paginator.page(1)
    except EmptyPage:
        funds = paginator.page(paginator.num_pages)


    context = {
        'user': request.user,
        'funds': funds,
        'date_from': request.GET.get('from'),
        'date_to': request.GET.get('to'),

    }
    return render_to_response(template_name, context)


@user_passes_test(is_staff, login_url='/admin')
@transaction.atomic
def accept_reject_funds(request, template_name='', title=''):
    fund_key = request.GET.get('key')
    action_pressed = request.GET.get('action')

    try:
        fund = Fund.objects.get(pk=fund_key)
    except ObjectDoesNotExist:
        return redirect(reverse('settlement_dashboard'))


    if action_pressed:
        if fund.status == 'p':
            if action_pressed == 'a':
                fund.requested_by.account_balance = fund.requested_by.account_balance + fund.amount
                fund.requested_by.save()
                fund.status = 'f'
            else:
                fund.status = 'p'
        fund.save()

    return redirect(reverse('settlement_funds'))

@user_passes_test(is_staff, login_url='/admin')
@transaction.atomic
def add_payment(request, template_name='', title=''):
    agent_pk = request.GET.get('agent_key')
    amount = request.GET.get('amount')

    try:
        agent = Agent.objects.get(pk=agent_pk)
    except ObjectDoesNotExist:
        return redirect(reverse('settlement_dashboard'))
    p = Payment()

    p.amount = Decimal(amount)
    p.agent = agent
    p.save()

    if agent.credit <= Decimal('0'):
        return redirect(reverse('settlement_dashboard'))
    agent.credit = agent.credit - p.amount
    agent.save()

    return redirect(reverse('settlement_payments'))


@user_passes_test(is_staff, login_url='/admin')
def payments_view(request, template_name='settlement/payments.html', title='Payments'):

    payments_list = Payment.objects.all()

    if request.GET.get('from') or request.GET.get('to'):
        from_ = request.GET.get('from').split('-')
        to_ = request.GET.get('to').split('-')
        if not request.GET.get('to'):
            from_date = date(int(from_[0]), int(from_[1]), int(from_[2]))
            payments_list = payments_list.filter(requested_on__gt=from_date)
        else:
            from_date = date(int(from_[0]), int(from_[1]), int(from_[2]))
            to_date = date(int(to_[0]), int(to_[1]), int(to_[2]))
            payments_list = payments_list.filter(requested_on__range=(from_date, to_date))


    paginator = Paginator(payments_list, 10)
    page = request.GET.get('page')

    try:
        payments = paginator.page(page)
    except PageNotAnInteger:
        payments = paginator.page(1)
    except EmptyPage:
        payments = paginator.page(paginator.num_pages)

    context = {
        'user': request.user,
        'payments': payments,
        'date_from': request.GET.get('from'),
        'date_to': request.GET.get('to'),

    }

    return render_to_response(template_name, context)

@user_passes_test(is_staff, login_url='/admin')
def mtos_view(request, template_name="settlement/mto.html", title=""):

    if request.method == "POST":
        form = MtoForm(request.POST)
        if form.is_valid():
            form.save()
            pass
        else:
            context = {
                'user': request.user,
                'mtos': Mto.objects.all(),
                'title': title,
                'form': form,
            }
            return render(request, template_name, context)

    mto_list = Mto.objects.all()
    mto_form = MtoForm()

    context = {
        'user': request.user,
        'mtos': mto_list,
        'title': title,
        'form': mto_form,
    }
    return render(request, template_name, context)

@user_passes_test(is_staff, login_url='/admin')
def mto_view(request, template_name="settlement/mto-view.html", title=""):
    mto_pk = request.GET.get('mto')
    form = ScheduleForm()
    if request.method == "POST":
        form = ScheduleForm(request.POST)
        if form.is_valid():
            form.save()
            mto_pk = str(request.POST.get('hidden_id'))
        else:
            mto_pk = str(request.POST.get('hidden_id'))


    if mto_pk:
        try:
            mto = Mto.objects.get(pk=mto_pk)
        except ObjectDoesNotExist:
            return redirect(reverse('settlement_dashboard'))
    else:
        return redirect(reverse('settlement_dashboard'))


    mto_schedules = Schedule.objects.filter(mto=mto)
    total_agents = Agent.objects.filter(mto=mto).count()
    total_credits = Agent.objects.filter(mto=mto).aggregate(Sum('credit'))


    context = {
        'mto': mto,
        'user': request.user,
        'schedules': mto_schedules,
        'total_agents': total_agents,
        'credits_sum': total_credits['credit__sum'],
        'form':form,
    }


    return render(request, template_name, context)

def login_view(request, template_name="settlement/login.html", title=""):

    if request.method == "POST":
        form = SettlementLoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')

            user = authenticate(username=username, password=password)

            if user and user.is_active:
                login(request, user)
                return HttpResponseRedirect(reverse('settlement_dashboard'))
            else:
                return HttpResponseRedirect(reverse('settlement_login'))
        else:
            return render(request, template_name, {'form':form})

    form = SettlementLoginForm()

    context = {
        'form': form,
    }
    return render(request, template_name, context)

@user_passes_test(is_staff, login_url='/admin')
def country_view(request, template_name="settlement/countries.html", title="Countries"):

    form = CountryForm()
    if request.method == "POST":
        form = CountryForm(request.POST)
        if form.is_valid():
            form.save()




    countries_list = Country.objects.all()


    context = {
        'user': request.user,
        'countries': countries_list,
        'form': form,

    }
    return render(request, template_name, context)


def rates_view(request, template_name="settlement/rates.html", title="Rates"):
    form = RateForm()

    if request.method == "POST":
        form = RateForm(request.POST)
        if form.is_valid():
            form.save()

    rates = Rate.objects.all()


    context = {
        'user': request.user,
        'rates': rates,
        'form': form,
    }
    return render(request, template_name, context)

def currencies_view(request, template_name="settlement/currencies.html", title=""):
    form = CurrencyForm()

    if request.method == "POST":
        form = CurrencyForm(request.POST)
        if form.is_valid():
            form.save()

    currency_list = Currency.objects.all()

    context = {
        'user': request.user,
        'currencies': currency_list,
        'form': form,

    }

    return render(request, template_name, context)

def edit_rate(request, template_name="settlement/edit-rate.html", title="Edit Rate"):

    pk = request.GET.get('id')

    if request.method == "POST":
        pk = request.POST.get('pkk')
        rate = Rate.objects.get(pk=pk)
        form = RateForm(request.POST, instance=rate)

        if form.is_valid():
            form.save()
        else:
            return render(request, template_name, {
                'title': title,
                'user': request.user,
                'rate': rate,
                'form': form,
            })

    if pk:
        try:
            rate = Rate.objects.get(pk=pk)
            form = RateForm(instance=rate)
        except ObjectDoesNotExist:
            return HttpResponseRedirect(reverse('settlement_dashboard'))
    else:
        return HttpResponseRedirect(reverse('settlement_dahsboard'))



    context = {
        'title': title,
        'user': request.user,
        'form': form,
        'rate':rate,
    }

    return render(request, template_name, context)

def edit_currency(request, template_name="settlement/edit-currency.html", title="Edit Rate"):

    pk = request.GET.get('id')

    if request.method == "POST":
        pk = request.POST.get('pkk')
        currency = Currency.objects.get(pk=pk)
        form = CurrencyForm(request.POST, instance=currency)

        if form.is_valid():
            form.save()
        else:
            return render(request, template_name, {
                'title': title,
                'user': request.user,
                'currency': currency,
                'form': form,
            })

    if pk:
        try:
            currency = Currency.objects.get(pk=pk)
            form = CurrencyForm(instance=currency)
        except ObjectDoesNotExist:
            return HttpResponseRedirect(reverse('settlement_dashboard'))
    else:
        return HttpResponseRedirect(reverse('settlement_dahsboard'))



    context = {
        'title': title,
        'user': request.user,
        'form': form,
        'currency':currency,
    }

    return render(request, template_name, context)
